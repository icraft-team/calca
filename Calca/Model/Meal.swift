//
//  Meal.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 08/05/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

import UIKit
import Gloss

class Meal: Gloss.Decodable, Equatable {
    var id : Int?
    var totalCals : Int = 0
    var numberOfGramsOrServings : Int?
    var dishID : Int?
    var dishName : String?
    var time : Int?
    var userID : Int?
    var singled : Bool?
    var date : Date?
    
    required init?(json: JSON) {
        self.id = "id" <~~ json
        
        if let totalCalories : Int = "totalCals" <~~ json {
            self.totalCals = totalCalories
        }
        
        self.numberOfGramsOrServings = "numberOfGramsOrServings" <~~ json
        self.dishID = "dishID" <~~ json
        self.dishName = "dishName" <~~ json
        self.time = "time" <~~ json
        self.userID = "userID" <~~ json
        self.singled = "singled" <~~ json
        
        if let dateString: String? = ("date" <~~ json) {
            self.date = Utils.getDateFrom(dateString, withFormat: "yyyy-MM-dd HH:mm:ss Z")
        }
    }
    
    static func == (m1: Meal, m2: Meal) -> Bool {
        return m1.id == m2.id
    }
    
    func delete () {
        guard let id = self.id else { print("[Meal] kekus maximus, check this"); return }
        
        AlamoMine.delete(REST.deleteMeal(id), json: nil) { (response) -> (Void) in
            if response?.statusCode == 200 {
                print("[Meal] succesfully deleted meal")
            }
        }
    }
}
