//
//  RestoreAuthRequest.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 15/03/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

import UIKit
import Gloss

class RestoreAuthRequest: Gloss.Encodable {
    var authToken: String?
    
    init(_ authToken: String) {
        self.authToken = authToken
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "authToken" ~~> self.authToken
            ])
    }
}
