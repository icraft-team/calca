//
//  RegistrationRequest.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 30/04/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

import UIKit
import Gloss

class RegistrationRequest: Gloss.Encodable {
    var login, pass, email: String?
    var timezoneOffset: Int?
    
    init(login: String, pass: String, email: String, timezoneOffset : Int) {
        self.login = login
        self.pass = pass
        self.email = email
        self.timezoneOffset = timezoneOffset
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "login" ~~> self.login,
            "pass" ~~> self.pass,
            "email" ~~> self.email,
            "timezoneOffset" ~~> self.timezoneOffset
            ])
    }
}
