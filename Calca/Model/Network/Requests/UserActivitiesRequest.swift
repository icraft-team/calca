//
//  MealListRequest.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 26/11/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

import UIKit
import Gloss

class UserActivitiesRequest: Gloss.Encodable {
    var startDateInterval: Int
    var endDateInterval: Int
    
    init(_ start: Int, _ end: Int) {
        self.startDateInterval = start
        self.endDateInterval = end
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "startDateInterval" ~~> self.startDateInterval,
            "endDateInterval" ~~> self.endDateInterval
            ])
    }
}
