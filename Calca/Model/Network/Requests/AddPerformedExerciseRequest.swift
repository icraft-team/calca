//
//  AddPerformedExerciseRequest.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 04/03/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

import UIKit
import Gloss

class AddPerformedExerciseRequest: Gloss.Encodable {
    var selectedExercise : Exercise?
    var minutes : Int?
    var epochDateSeconds : Int?
    
    init(_ selectedExercise : Exercise,_ minutes : Int,_ date: Date) {
        self.selectedExercise = selectedExercise
        self.minutes = minutes
        self.epochDateSeconds = Int(date.timeIntervalSince1970)
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "selectedExercise" ~~> self.selectedExercise,
            "minutes" ~~> self.minutes,
            "epochDateSeconds" ~~> self.epochDateSeconds
            ])
    }
}
