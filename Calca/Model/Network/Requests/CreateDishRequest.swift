//
//  CreateMealRequest.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 06/05/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

import UIKit
import Gloss

class CreateDishRequest: Gloss.Encodable {
    var name : String
    var calsPer : Int
    var singled : Bool
    
    init(name : String, calsPer : Int, singled : Bool) {
        self.name = name
        self.calsPer = calsPer
        self.singled = singled
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "name" ~~> self.name,
            "calsPer" ~~> self.calsPer,
            "singled" ~~> self.singled
            ])
    }
}
