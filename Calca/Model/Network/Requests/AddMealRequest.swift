//
//  AddMealRequest.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 08/05/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

import UIKit
import Gloss

class AddMealRequest: Gloss.Encodable {
    var selectedDish : Dish?
    var numberOfGramsOrServings : Int
    var secondsBeforeThisMealSince1970 : Int
    
    init(_ selectedDish : Dish,_ numberOfGramsOrServings : Int,_ date: Date) {
        self.selectedDish = selectedDish
        self.numberOfGramsOrServings = numberOfGramsOrServings
        self.secondsBeforeThisMealSince1970 = Int(date.timeIntervalSince1970)
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "selectedDish" ~~> self.selectedDish,
            "numberOfGramsOrServings" ~~> self.numberOfGramsOrServings,
            "secondsBeforeThisMealSince1970" ~~> self.secondsBeforeThisMealSince1970
            ])
    }
}
