//
//  AuthRequest.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 01/05/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

import UIKit
import Gloss

struct AuthRequest: Gloss.Encodable {
    var login: String?
    var pass: String?
    
    init(login: String, pass: String) {
        self.login = login
        self.pass = pass
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "login" ~~> self.login,
            "pass" ~~> self.pass
            ])
    }
}
