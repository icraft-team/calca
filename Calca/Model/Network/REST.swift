//
//  CalcaURL.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 14/03/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

import UIKit

struct REST {
    static var backend: String {
        if let path = Bundle.main.path(forResource: "Info", ofType: "plist"),
           let settings = NSDictionary(contentsOfFile: path),
           let backendString = settings["backend"] as? String {
            return backendString
        } else {
            print("[REST] cannot load backend address from settings, please add this string to Info.plist with 'backend' key")
            return ""
        }
    }
    
    static let registration = backend + "/registration"
    static let auth = backend + "/auth"
    static let restoreAuth = backend + "/restoreAuth"
    static let syncUser = backend + "/syncUser"
    static let userActivities = backend + "/userActivities"
    static let achievements = backend + "/achievements"
    
    static let addMeal = backend + "/addMeal"
    static func deleteMeal(_ mealID: Int) -> String {
        return backend + "/deleteMeal/" + String(mealID)
    }
    static func mealAutocomplete(_ searchString: String) -> String {
        return backend + "/mealAutocomplete/" + searchString
    }
    
    static let createDish = backend + "/createDish"
    
    static let addPerformedExercise = backend + "/addPerformedExercise"
    static func deletePerformedExercise(_ exerciseID: Int) -> String {
        return backend + "/deletePerformedExercise/" + String(exerciseID)
    }
    static func exerciseAutocomplete(_ searchString: String) -> String {
        return backend + "/exerciseAutocomplete/" + searchString
    }
    
    static func loadImage(_ imageName: String) -> String {
        return backend + "/images/" + imageName
    }
}
