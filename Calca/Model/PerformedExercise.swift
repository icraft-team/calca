//
//  PerformedExercise.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 04/03/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

import UIKit
import Gloss

class PerformedExercise: Glossy, Equatable {
    var id : Int?
    var exerciseID : Int?
    var name : String?
    var caloriesBurnt : Int = 0
    var userID : Int?
    var time : Int?
    var date : Date?
    
    required init?(json: JSON) {
        self.id = "id" <~~ json
        self.exerciseID = "exerciseID" <~~ json
        self.name = "name" <~~ json
        
        if let calories : Int = "caloriesBurnt" <~~ json {
            self.caloriesBurnt = calories
        }
        
        self.userID = "userID" <~~ json
        self.time = "time" <~~ json
        
        if let dateString: String? = ("date" <~~ json) {
            self.date = Utils.getDateFrom(dateString, withFormat: "yyyy-MM-dd HH:mm:ss Z")
        }
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> self.id,
            "exerciseID" ~~> self.exerciseID,
            "name" ~~> self.name,
            "caloriesBurnt" ~~> self.caloriesBurnt,
            "userID" ~~> self.userID,
            "time" ~~> self.time
            ])
    }
    
    static func == (p1: PerformedExercise, p2: PerformedExercise) -> Bool {
        return p1.id == p2.id
    }
    
    func delete() {
        guard let id = self.id else {
            print("[PerformedExercise] id is nil, wtf")
            return
        }
        
        AlamoMine.delete(REST.deletePerformedExercise(id), json: nil) { (response) -> (Void) in
            if response?.statusCode == 200 {
                print("[PerformedExercise] succesfully deleted exercise")
            }
        }
    }
}
