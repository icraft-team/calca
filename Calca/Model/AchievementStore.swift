//
//  AchievementStore.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 23/03/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

import UIKit
import Gloss

struct AchievementStore: Gloss.Decodable {
    let list:[Achievement]?
    
    init?(json: JSON) {
        self.list = "list" <~~ json
    }
    
    static func fetch(completion: @escaping (AchievementStore?) -> (Void)) {
        AlamoMine.get(REST.achievements) { (response) -> (Void) in
            guard let json = response?.json else { print("[AchievementStore] json is nil"); return }
            let store = AchievementStore(json: json)
            completion(store)
        }
    }
}
