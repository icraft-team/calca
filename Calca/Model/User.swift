//
//  User.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 30/04/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

import UIKit
import Gloss

struct User : Glossy {
    var id: Int?
    var login: String?
    var phone: String?
    var weight: Float?
    var caloriesPerDay: Int? = 0
    var registrationDate: Int?
    var email: String?
    var metricSystem: Bool?
    var gold: Int?
    var timezoneOffset: Int?
    
    init?(json: JSON) {
        self.id = "id" <~~ json
        self.login = "login" <~~ json
        self.phone = "phone" <~~ json
        self.caloriesPerDay = "caloriesPerDay" <~~ json
        self.registrationDate = "registrationDate" <~~ json
        self.email = "email" <~~ json
        self.weight = "weight" <~~ json
        self.metricSystem = "metricSystem" <~~ json
        self.gold = "gold" <~~ json
        self.timezoneOffset = "timezoneOffset" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> self.id,
            "login" ~~> self.login,
            "phone" ~~> self.phone,
            "weight" ~~> self.weight,
            "caloriesPerDay" ~~> self.caloriesPerDay,
            "registrationDate" ~~> self.registrationDate,
            "email" ~~> self.email,
            "metricSystem" ~~> self.metricSystem,
            "gold" ~~> self.gold,
            "timezoneOffset" ~~> self.timezoneOffset
            ])
    }
    
    mutating func sync() {
        self.timezoneOffset = NSTimeZone.local.secondsFromGMT()
        AlamoMine.post(REST.syncUser, json: self.toJSON()) { (response) -> (Void) in
            if response?.statusCode == 200 {
                print("[User] sync success")
            }
        }
    }
    
    var weightInKilos : Float {
        guard let weight = self.weight else { print("weight is nil, check"); return -1 }
        guard let metric = self.metricSystem else { return -1 }
        
        if metric {
            return weight
        } else {
            return weight / 2.2
        }
    }
}
