//
//  Achievement.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 20/03/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

import UIKit
import Gloss

struct Achievement: Glossy {
    let name: String?
    let description: String?
    let goldReward: Int?
    let image: String?
    let total: Int?
    let daily: Bool?
    let title: String?
    let local: Bool?
    let rewardClaimed: Bool?
    let progress: Float?
    
    init?(json: JSON) {
        self.name = "name" <~~ json
        self.description = "description" <~~ json
        self.goldReward = "goldReward" <~~ json
        self.image = "image" <~~ json
        self.total = "total" <~~ json
        self.daily = "daily" <~~ json
        self.title = "title" <~~ json
        self.local = "local" <~~ json
        self.rewardClaimed = "rewardClaimed" <~~ json
        self.progress = "progress" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "name" ~~> self.name,
            "description" ~~> self.description,
            "goldReward" ~~> self.goldReward,
            "image" ~~> self.image,
            "total" ~~> self.total,
            "daily" ~~> self.daily,
            "title" ~~> self.title,
            "local" ~~> self.local,
            "rewardClaimed" ~~> self.rewardClaimed,
            "progress" ~~> self.progress
            ])
    }
}
