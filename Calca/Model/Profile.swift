//
//  Profile.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 30/04/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

import UIKit
import Gloss

let TODAY_MEALS_UPDATE_EVENT = "meals update event"

struct Profile: Gloss.Decodable {
    var user : User?
    var authToken : String?
    
    init?(json: JSON) {
        self.user = "user" <~~ json
        self.authToken = "authToken" <~~ json
        Cache.get().save(self.authToken, forKey: AUTH_TOKEN_KEY)
    }
}
