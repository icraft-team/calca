//
//  Meal.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 01/05/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

import UIKit
import Gloss

class Dish: Glossy {
    var id : Int?
    var name : String?
    var calsPer : Int?
    var singled : Bool?
    var pictureURL : String?
    
    required init?(json: JSON) {
        self.id = "id" <~~ json
        self.name = "name" <~~ json
        self.calsPer = "calsPer" <~~ json
        self.singled = "singled" <~~ json
        self.pictureURL = "pictureURL" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> self.id,
            "name" ~~> self.name,
            "calsPer" ~~> self.calsPer,
            "singled" ~~> self.singled,
            "pictureURL" ~~> self.pictureURL
            ])
    }
}
