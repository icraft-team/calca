//
//  Exercise.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 08/05/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

import UIKit
import Gloss

struct Exercise: Glossy {
    var id : Int?
    var name : String?
    var tags : String?
    var MET : Float?
    
    init?(json: JSON) {
        self.id = "id" <~~ json
        self.name = "name" <~~ json
        self.tags = "tags" <~~ json
        self.MET = "MET" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> self.id,
            "name" ~~> self.name,
            "tags" ~~> self.tags,
            "MET" ~~> self.MET
            ])
    }
    
    func perform(minutes: Int, date: Date, completion: @escaping (Response?) -> (Void)) {
        let request = AddPerformedExerciseRequest(self, minutes, date)
        
        AlamoMine.post(REST.addPerformedExercise, json: request.toJSON()) { (response) -> (Void) in
            completion(response)
        }
    }
}

