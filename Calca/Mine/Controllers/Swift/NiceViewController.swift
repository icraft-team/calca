//
//  CoolViewController.swift
//  KingMFC
//
//  Created by Valentin Cherepyanko on 19/04/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

import UIKit
import SideMenu

let Cards = "showCards"

class NiceViewController: UIViewController {
    
    @IBInspectable var hideNavigationBar: Bool = false
    @IBInspectable var blurBackground: Bool = false
    
    @IBOutlet var fieldsToCheck: [UITextField]!
    
    var effectView: UIVisualEffectView!
    var segueObjects : NSMutableDictionary! = NSMutableDictionary()
    var animator: UIViewPropertyAnimator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if (blurBackground) { blur() }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if (hideNavigationBar) {
            navigationController?.setNavigationBarHidden(hideNavigationBar, animated: true)
        }
    }
    
    func blur() {
        let screenshot = Utils.takeScreenshot()
        let imageView = UIImageView.init(frame: self.view.bounds)
        imageView.image = screenshot
        self.view.backgroundColor = UIColor.clear
        self.view.insertSubview(imageView, at: 0)
        UIView.animate(withDuration: 0.5) {
            imageView.addBlur()
        }
        imageView.fit(in: self.view)
    }
    
    func checkFields() -> Bool {
        var allOk = true
        for field in fieldsToCheck {
            if field.text?.count == 0 && field.superview?.alpha == 1 {
                field.bounce()
                allOk = false
            }
        }
        
        return allOk
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }

    // segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let toViewController = segue.destination as? NiceViewController {
            toViewController.passData(segueObjects.object(forKey: segue))
        }
    }
    
    func passData(_ object: Any?) {
        
    }
    
    func segue(_ segue: String) {
        self.segue(segue, object: nil)
    }
    
    func segue(_ segue: String, object: Any?) {
        if (object != nil) {
            segueObjects.setObject(object!, forKey: segue as NSCopying)
        }
        
        self.performSegue(withIdentifier: segue, sender: self)
    }
    
    func post(_ url: String, _ json: JSON?, _ completion: @escaping (Response?) -> Void) {
        AlamoMine.post(url, json: json) { (response) -> (Void) in
            if (response?.statusCode == 401) {
                self.segue("auth")
            } else {
                completion(response)
            }
        }
    }
    
    func get(_ url: String,  _ completion: @escaping (Response?) -> Void) {
        AlamoMine.get(url) { (response) -> (Void) in
            if (response?.statusCode == 401) {
                self.segue("auth")
            } else {
                completion(response)
            }
        }
    }
    
    func close() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func close(_ sender: Any) {
        self.close()
//        self.navigationController?.popViewController(animated: true)
    }
}

typealias NiceViewControllerVFX = NiceViewController
extension NiceViewControllerVFX {
//    func blurBack() {
//        let imageView = UIImageView.init(frame: (UIApplication.shared.keyWindow?.frame)!)
//        imageView.image = UIApplication.shared.screenShot
//        effectView = UIVisualEffectView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
//        self.view.insertSubview(imageView, at: 0)
//        imageView.insertSubview(effectView, at: 0)
//
//        animator = UIViewPropertyAnimator(duration: 1, curve: .linear) {
//            self.effectView.effect = nil
//        }
//
//        UIView.animate(withDuration: 2) {
//            self.animator?.fractionComplete = 1
//        }
//    }
}
