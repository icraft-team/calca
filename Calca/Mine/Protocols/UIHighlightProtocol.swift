//
//  UIHighlightProtocol.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 19/03/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

import UIKit

protocol UIHighlightProtocol {
    func highlight(_ highlighted: Bool)
}
