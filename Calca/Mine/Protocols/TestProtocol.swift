//
//  TestProtocol.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 14/03/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

import UIKit

protocol TestProtocol {
    func test() -> Bool
}
