//
//  AnimatedViewController.h
//  Conf
//
//  Created by Valentin Cherepyanko on 31/08/15.
//  Copyright (c) 2015 Valentin Cherepyanko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AnimationHelper.h"
#import "AdjustableBlurEffect.h"

@interface UIView (Animations)

#define ANIMATION_DURATION 0.4

- (void)hide;
- (void)show;
- (void)show:(BOOL)show;
- (void)showAnimated:(BOOL)show;
- (void)hideAnimated;
- (void)showAnimated;
- (void)hideAnimatedWithCompletion:(void (^)())callback;
- (void)showAnimatedWithCompletion:(void (^)())callback;

- (void)popup;
- (void)popdown;

- (void)move:(CGPoint)point;
- (void)addBlur;
- (void)blurAnimated;
- (void)bounce;
- (void)matchCenterWith:(id)object;
- (void)flipOpacity;
- (void)addShadow;

- (void)fade;
- (void)unfade;
- (void)zoomfade;
- (void)fade:(void (^)())callback;
- (void)unfade:(void (^)())callback;
- (void)zoomfade:(void (^)())callback;
@end
