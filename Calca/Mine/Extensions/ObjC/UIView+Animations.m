//
//  AnimatedViewController.m
//  Conf
//
//  Created by Valentin Cherepyanko on 31/08/15.
//  Copyright (c) 2015 Valentin Cherepyanko. All rights reserved.
//

#import "UIView+Animations.h"

@implementation UIView (Animations)

- (void)hide {
    self.alpha = 0;
}

- (void)show {
    self.alpha = 1;
}

- (void)flipOpacity {
    if (self.alpha == 1) {
        [self hideAnimated];
    } else {
        [self showAnimated];
    }
}

- (void)show:(BOOL)show {
    if (show) {
        [self show];
    } else {
        [self hide];
    }
}

- (void)showAnimated:(BOOL)show {
    if (show) {
        [self showAnimated];
    } else {
        [self hideAnimated];
    }
}

- (void)hideAnimated {
    if (self.alpha > 0) {
        self.alpha = 1;
        [UIView animateWithDuration:ANIMATION_DURATION animations:^{
            self.alpha = 0;
        } completion:nil];
    }
}

- (void)showAnimated {
    if (self.alpha < 1) {
        self.alpha = 0;
        [UIView animateWithDuration:ANIMATION_DURATION animations:^{
            self.alpha = 1;
        } completion:nil];
    }
}

- (void)hideAnimatedWithCompletion:(void (^)())callback {
    self.alpha = 1;
    [UIView animateWithDuration:ANIMATION_DURATION animations:^{
        self.alpha = 0;
    } completion:^(BOOL completed) {
        callback();
    }];
}

- (void)showAnimatedWithCompletion:(void (^)())callback {
    self.alpha = 0;
    [UIView animateWithDuration:ANIMATION_DURATION animations:^{
        self.alpha = 1;
    } completion:^(BOOL completed) {
        callback();
    }];
}

- (void)popup {
    POPSpringAnimation *anim = [AnimationHelper popupAnimation];
    [self pop_addAnimation:anim forKey:@"popup"];
}

- (void)popdown {
    POPSpringAnimation *anim = [AnimationHelper popdownAnimation];
    [self pop_addAnimation:anim forKey:@"popdown"];
}

- (void)move:(CGPoint)point {
    [UIView animateWithDuration:ANIMATION_DURATION animations:^{
        self.frame = CGRectOffset(self.frame, point.x, point.y);
    }];
}

- (void)addBlur {
    UIBlurEffect *effect = [AdjustableBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *beView = [[UIVisualEffectView alloc] initWithEffect:effect];
    beView.frame = self.bounds;
    
    self.backgroundColor = [UIColor clearColor];
    [self insertSubview:beView atIndex:0];
}

- (void)bounce {
    POPSpringAnimation *anim = [AnimationHelper bounceAnimation];
    [self pop_addAnimation:anim forKey:@"bounce"];
}

- (void)matchCenterWith:(id)object {
    UIView *view = (UIView *)object;
    
    [UIView animateWithDuration:ANIMATION_DURATION delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.center =  CGPointMake(view.center.x, self.center.y);
    } completion:nil];
}

- (void)addShadow {
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOpacity = 0.13;
    self.layer.shadowRadius = 1;
    self.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
}

- (void)fade {
    [self hideAnimated];
    POPSpringAnimation *anim = [AnimationHelper fadeAnimation];
    [self pop_addAnimation:anim forKey:@"fade"];
}

- (void)unfade {
    [self showAnimated];
    POPSpringAnimation *anim = [AnimationHelper unfadeAnimation];
    [self pop_addAnimation:anim forKey:@"unfade"];
}

- (void)zoomfade {
    [self hideAnimated];
    POPSpringAnimation *anim = [AnimationHelper zoomAnimation];
    [self pop_addAnimation:anim forKey:@"fade"];
}

- (void)fade:(void (^)())callback {
    [self hideAnimated];
    POPSpringAnimation *anim = [AnimationHelper fadeAnimation];
    anim.completionBlock = ^(POPAnimation *anim, BOOL finished) {
        callback();
    };
    [self pop_addAnimation:anim forKey:@"fade"];
}

- (void)unfade:(void (^)())callback {
    [self showAnimated];
    POPSpringAnimation *anim = [AnimationHelper unfadeAnimation];
    anim.completionBlock = ^(POPAnimation *anim, BOOL finished) {
        callback();
    };
    [self pop_addAnimation:anim forKey:@"unfade"];
}

- (void)zoomfade:(void (^)())callback {
    [self hideAnimated];
    POPSpringAnimation *anim = [AnimationHelper zoomAnimation];
    anim.completionBlock = ^(POPAnimation *anim, BOOL finished) {
        callback();
    };
    [self pop_addAnimation:anim forKey:@"fade"];
}

- (void)blurAnimated {
    UIVisualEffectView *view = [[UIVisualEffectView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    UIBlurEffect *effect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    view.effect = effect;
    [UIView animateWithDuration:3 animations: ^{
        [self addSubview:view];
    } completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
