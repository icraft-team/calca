//
//  FoundationExtensions.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 08/05/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

import UIKit

let EVENT : String! = "event"

extension NSObject {
    func subscribe(_ eventName : String, _ selector : Selector) {
        NotificationCenter.default.addObserver(self, selector: selector, name: NSNotification.Name(rawValue: eventName), object: nil)
    }
    
    func unsubscribe() {
        NotificationCenter.default.removeObserver(self)
    }
    
    func log(_ string : String) {
        let description = String(describing: type(of: self)) as String
        print("[" + description + "] " + string)
    }
}

extension NotificationCenter {
    static func post(_ eventName : String) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: eventName), object: nil)
    }
    
    static func post(_ eventName : String, _ object : Any) {
        let dataDict : [String : Any] = ["object" : object]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: eventName), object: nil, userInfo: dataDict)
    }
}

typealias UnixTime = Int
extension UnixTime {
    private func formatType(form: String) -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateFormat = form
        return dateFormatter
    }
    var dateFull: Date {
        return Date(timeIntervalSince1970: Double(self))
    }
    var toHour: String {
        return formatType(form: "HH:mm").string(from: dateFull)
    }
    var toDay: String {
        return formatType(form: "MM/dd/yyyy").string(from: dateFull)
    }
}

extension Array where Element: Equatable {
    // Remove first collection element that is equal to the given `object`:
    mutating func remove(object: Element) {
        if let index = index(of: object) {
            remove(at: index)
        }
    }
}

extension Date {
    enum Time {
        case Day
        case Month
    }
    
    var startOfDay: Date {
        return Calendar.current.startOfDay(for: self)
    }
    
    var endOfDay: Date {
        var components = DateComponents()
        components.day = 1
        components.second = -1
        
        guard let date = Calendar.current.date(byAdding: components, to: startOfDay) else {
            print("[Date] can't get end of the day, please check")
            return Date()
        }
        return date
    }
    
    var isToday: Bool {
        return NSCalendar.current.isDateInToday(self)
    }
    
    func stringWith(format: String) -> String {
        return Utils.string(from: self, withFormat: format)
    }
    
    var day: Int {
        return Calendar.current.component(.day, from: self)
    }
    
    var month: Int {
        return Calendar.current.component(.month, from: self)
    }
    
    var year: Int {
        return Calendar.current.component(.year, from: self)
    }
    
    var incrementedByDay: Date? {
        return Calendar.current.date(byAdding: .day, value: 1, to: self)
    }
    
    var decrementedByDay: Date? {
        return Calendar.current.date(byAdding: .day, value: -1, to: self)
    }
    
    func subtractDays(_ daysCound: Int) -> Date? {
        return Calendar.current.date(byAdding: .day, value: -daysCound, to: self)
    }
    
    func addDays(_ daysCound: Int) -> Date? {
        return Calendar.current.date(byAdding: .day, value: daysCound, to: self)
    }
}

precedencegroup MultiplicationPrecedence {
    associativity: left
    higherThan: AdditionPrecedence
}
infix operator !≈≈ : MultiplicationPrecedence
infix operator ≈≈ : MultiplicationPrecedence
infix operator ≈ : MultiplicationPrecedence

// returns true if dates have same year, month and day
func ≈≈(left: Date, right: Date) -> Bool {
    return  left.day == right.day &&
            left.month == right.month &&
            left.year == right.year
}

func !≈≈(left: Date, right: Date) -> Bool { return !(left ≈≈ right) }

// assign right to the left if left date and right date have same year, month and day
func ≈(left: inout Date, right: Date) {
    if left !≈≈ right {
        left = right
    }
}

extension TimeInterval {
    var toInt: Int {
        return Int(self)
    }
}

extension Float {
    var toString: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}

extension UIApplication {
    var screenShot: UIImage?  {
        if let layer = keyWindow?.layer {
            let scale = UIScreen.main.scale
            
            UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, scale);
            if let context = UIGraphicsGetCurrentContext() {
                layer.render(in: context)
                let screenshot = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
                return screenshot
            }
        }
        return nil
    }
}

extension String {
    static func caseFor(_ quantity: Int, form1: String, form2: String, form5: String) -> String {
        let div100 = quantity % 100
        let div10 = quantity % 10
        if (div100 > 10 && div100 < 20) { return form5 }
        if (div10 > 1 && div10 < 5) { return form2 }
        if (div10 == 1) { return form1 }
        return form5
    }
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    var toFloat : Float {
        if let float = Float(self) {
            return float
        } else {
            print("[StringExtension] can't get float value from this string")
            return 0
        }
    }
    
    var toInt : Int {
        if let int = Int(self) {
            return int
        } else {
            print("[StringExtension] can't get int value from this string")
            return 0
        }
    }
}

extension URL {
    func get(callback: (NiceResponse) -> ()) {
        
        
//        callback()
    }
    
//    func post(json: String, callback: (NiceResponse) -> ()) {
//        var request = URLRequest(url: self)
//        request.httpMethod = "POST"
//        request.httpBody = json.data(using: String.Encoding.utf8)
//        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
//        request.addValue("application/json", forHTTPHeaderField: "Accept")
//        
//        let task = URLSession.shared.dataTask(with: request) {(data, response, error) in
//            let niceResponse = NiceResponse()
//            if let bites = data {
//                niceResponse.dataString = String(data: bites, encoding: String.Encoding.utf8)
//            }
//        }
//    }
}
