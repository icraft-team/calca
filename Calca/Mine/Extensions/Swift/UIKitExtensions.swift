//
//  UIKitExtensions.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 07/05/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

import UIKit

extension NSLayoutConstraint {
    func setValueAnimated(_ value : CGFloat) {
        self.constant = value
        UIView.animate(withDuration: 0.4) { 
            let view : UIView = self.firstItem as! UIView
            view.superview?.layoutIfNeeded()
        }
    }
    
    func setValueAnimated(_ value : CGFloat, _ layoutView : UIView) {
        if self.constant == value { return }
        self.constant = value
        UIView.animate(withDuration: 0.4) {
            layoutView.layoutIfNeeded()
        }
    }
}

extension UIView {
    func addGradient(_ c1: UIColor, _ c2: UIColor) {
        let gradient: CAGradientLayer = CAGradientLayer()
        
        gradient.colors = [c1, c2]
        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
//        gradient.frame = CGRect(x: 0.0, y: 0.0, width: self.frame.size.width, height: self.frame.size.height)
        gradient.frame = self.bounds
        
        self.layer.insertSublayer(gradient, at:0)
    }
    
    func addShadow(radius: CGFloat, opacity: Float) {
        self.layer.shadowRadius = radius
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowOpacity = opacity
    }
    
    public var width: CGFloat {
        get { return self.frame.size.width }
        set { self.frame.size.width = newValue }
    }
    
    public var height: CGFloat {
        get { return self.frame.size.height }
        set { self.frame.size.height = newValue }
    }
    
    public var origin: CGPoint {
        set { self.frame.origin = newValue }
        get { return self.frame.origin }
    }
    public var size: CGSize {
        set { self.frame.size = newValue }
        get { return self.frame.size }
    }
    
    public var minX: CGFloat {
        return origin.x
    }
    
    public var maxX: CGFloat {
        return origin.x + size.width
    }
    
    public var minY: CGFloat {
        return origin.y
    }
    
    public var maxY: CGFloat {
        return origin.y + size.height
    }
}

extension UITextField {
    var isEmpty : Bool {
        if let empty = self.text?.isEmpty {
            return empty
        } else {
            return false
        }
    }
}

extension UISegmentedControl {
    func setFont(_ fontName: String, state: UIControlState, color: UIColor = .black, size: CGFloat = 16) {
        guard let font = UIFont(name:fontName, size:size) else {
            print("[UISegmentedControl] this font doesn't exist in project")
            return
        }
    setTitleTextAttributes([NSAttributedStringKey.font:font,NSAttributedStringKey.foregroundColor:color], for:state)
    }
    
    func removeDividers() {
        setDividerImage(UIImage().colored(with: .clear, size: CGSize(width: 1, height: 1)), forLeftSegmentState: .normal, rightSegmentState: .normal, barMetrics: .default)
//        setBackgroundImage(UIImage().colored(with: .clear, size: CGSize(width: 1, height: height)), for: .normal, barMetrics: .default)
//        setBackgroundImage(UIImage().colored(with: UIColor.init(red: 215/255.0, green: 0.0, blue: 30/255.0, alpha: 1.0), size: CGSize(width: 1, height: height)), for: .selected, barMetrics: .default);
        
//        for  borderview in subviews {
//            let upperBorder: CALayer = CALayer()
//            upperBorder.backgroundColor = UIColor.init(red: 215/255.0, green: 0.0, blue: 30/255.0, alpha: 1.0).cgColor
//            upperBorder.frame = CGRect(x: 0, y: borderview.frame.size.height-1, width: borderview.frame.size.width, height: 1)
//            borderview.layer.addSublayer(upperBorder)
//        }
    }
}

extension UIImage {
    func colored(with color: UIColor, size: CGSize) -> UIImage {
        UIGraphicsBeginImageContext(size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor);
        let rect = CGRect(origin: CGPoint(x: 0, y: 0), size: size)
        context!.fill(rect);
        let image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return image!
    }
}

extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { 
                self.image = image
            }
            }.resume()
    }
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
}

