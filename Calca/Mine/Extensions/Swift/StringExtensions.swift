//
//  StringExtensions.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 30/04/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

import UIKit

extension String {
    func toJSON() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func toJSONArray() -> [[String: Any]]? {
        if let data = self.data(using: .utf8) {
            if let json = (try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions(rawValue: 0))) as? [[String : AnyObject]] {
                return json
            } else {
                return nil
            }
        } else {
            return nil
        }
        
    }
}
