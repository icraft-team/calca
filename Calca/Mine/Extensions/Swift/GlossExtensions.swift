//
//  EncodableExtensions.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 01/05/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

import UIKit
import Gloss

extension Gloss.Encodable {
    func toJSONString() -> String? {
        let jsonDict = self.toJSON()
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonDict as Any, options: .prettyPrinted)
            
            return String(data: jsonData, encoding: String.Encoding.utf8)
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
}

//extension Decodable {
//    convenience init?(JSONString : String) {
//        self.init(JSONString.toJSON())
//        
////        if let data = JSONString.data(using: .utf8) {
////            do {
////                try init(JSONSerialization.jsonObject(with: data, options: []) as? [String: Any])
////            } catch {
////                print(error.localizedDescription)
////            }
////        }
//    }
//}
