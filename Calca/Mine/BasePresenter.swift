//
//  BasePresenter.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 14/07/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

import UIKit

protocol UserView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
}

class BasePresenter {
    
}
