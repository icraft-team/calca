//
//  ServerSettings.m
//  DEM
//
//  Created by Valentin Cherepyanko on 04/12/14.
//
//

#import "ServerSettings.h"

@implementation ServerSettings

+ (NSString *)getServerAddressString {
    if (HTTP_PORT > 0 && HTTP_PORT < 65535) {
        return [NSString stringWithFormat:@"http://%@:%d", SERVER_ADDRESS, HTTP_PORT];
    } else {
        return [NSString stringWithFormat:@"http://%@", SERVER_ADDRESS];
    }
}

+ (NSURL *)registrationURL {
    NSString *urlString = [NSString stringWithFormat:@"%@/%@", [self getServerAddressString], REGISTRATION_URL];
    return [NSURL URLWithString:urlString];
}

+ (NSURL *)authURL {
    NSString *urlString = [NSString stringWithFormat:@"%@/%@", [self getServerAddressString], AUTH_URL];
    return [NSURL URLWithString:urlString];
}

+ (NSURL *)authTokenURL:(NSString *)authToken {
    NSString *urlString = [NSString stringWithFormat:@"%@/%@/%@", [self getServerAddressString], AUTH_URL, authToken];
    return [NSURL URLWithString:urlString];
}

+ (NSURL *)mealAutocompleteURL:(NSString *)searchString {
    NSString *urlString = [NSString stringWithFormat:@"%@/%@/%@", [self getServerAddressString], MEAL_AUTOCOMPLETE_URL, searchString];
    return [NSURL URLWithString:[urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
}
    
+ (NSURL *)createDishURL {
    NSString *urlString = [NSString stringWithFormat:@"%@/%@", [self getServerAddressString], CREATE_DISH_URL];
    return [NSURL URLWithString:urlString];
}

+ (NSURL *)addMealURL {
    NSString *urlString = [NSString stringWithFormat:@"%@/%@", [self getServerAddressString], ADD_MEAL_URL];
    return [NSURL URLWithString:urlString];
}

+ (NSURL *)deleteMealURL:(long)mealID {
    NSString *urlString = [NSString stringWithFormat:@"%@/%@/%@", [self getServerAddressString], DELETE_MEAL_URL, [NSString stringWithFormat:@"%ld", mealID]];
    return [NSURL URLWithString:urlString];
}

+ (NSURL *)userSyncURL {
    NSString *urlString = [NSString stringWithFormat:@"%@/%@", [self getServerAddressString], USER_SYNC_URL];
    return [NSURL URLWithString:urlString];
}

+ (NSURL *)userActivitiesURL {
    NSString *urlString = [NSString stringWithFormat:@"%@/%@", [self getServerAddressString], USER_ACTIVITIES_URL];
    return [NSURL URLWithString:urlString];
}

+ (NSURL *)exerciseAutocompleteURL:(NSString *)searchString {
    NSString *urlString = [NSString stringWithFormat:@"%@/%@/%@", [self getServerAddressString], EXERCISE_AUTOCOMPLETE_URL, searchString];
    return [NSURL URLWithString:[urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
}

+ (NSURL *)addPerformedExerciseURL {
    NSString *urlString = [NSString stringWithFormat:@"%@/%@", [self getServerAddressString], ADD_PERFORMED_EXERCISE_URL];
    return [NSURL URLWithString:urlString];
}

+ (NSURL *)deletePerformedExerciseURL:(long)exerciseID {
    NSString *urlString = [NSString stringWithFormat:@"%@/%@/%@", [self getServerAddressString], DELETE_PERFORMED_EXERCISE_URL, [NSString stringWithFormat:@"%ld", exerciseID]];
    return [NSURL URLWithString:urlString];
}
@end
