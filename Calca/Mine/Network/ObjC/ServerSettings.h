//
//  ServerSettings.h
//  DEM
//
//  Created by Valentin Cherepyanko on 04/12/14.
//
//

//#define SERVER_ADDRESS @"localhost:10020"
#define SERVER_ADDRESS @"192.168.1.128:10020"
#define HTTP_PORT 0

#define REGISTRATION_URL @"registration"
#define AUTH_URL @"auth"
#define MEAL_AUTOCOMPLETE_URL @"mealAutocomplete"
#define CREATE_DISH_URL @"createDish"
#define ADD_MEAL_URL @"addMeal"
#define DELETE_MEAL_URL @"deleteMeal"
#define USER_SYNC_URL @"syncUser"
#define ADD_PERFORMED_EXERCISE_URL @"addPerformedExercise"
#define EXERCISE_AUTOCOMPLETE_URL @"exerciseAutocomplete"
#define USER_ACTIVITIES_URL @"userActivities"
#define DELETE_PERFORMED_EXERCISE_URL @"deletePerformedExercise"
#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface ServerSettings : NSObject

//+ (NSURL *)registrationURL;
//+ (NSURL *)authURL;
//+ (NSURL *)authTokenURL:(NSString *)authToken;
//+ (NSURL *)mealAutocompleteURL:(NSString *)searchString;
//+ (NSURL *)createDishURL;
//+ (NSURL *)addMealURL;
//+ (NSURL *)deleteMealURL:(long)mealID;
//+ (NSURL *)deletePerformedExerciseURL:(long)exerciseID;
//+ (NSURL *)userSyncURL;
//+ (NSURL *)userActivitiesURL;
//+ (NSURL *)addPerformedExerciseURL;
//+ (NSURL *)exerciseAutocompleteURL:(NSString *)searchString;

@end
