//
//  ServerRequestManager.m
//  DEM
//
//  Created by Valentin Cherepyanko on 03/12/14.
//
//

#import "ServerRequestManager.h"

@implementation ServerRequestManager

- (void)GETRequestWithURL:(NSURL *)url completion:(void (^)(NiceResponse *response))callback {
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            NiceResponse *niceResponse = [NiceResponse new];
            niceResponse.dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            niceResponse.urlResponse = response;
            niceResponse.statusCode = httpResponse.statusCode;
            callback(niceResponse);
        });
    }] resume];
}

- (void)POSTRequestWithURL:(NSURL *)url andData:(NSString *)dataString completion:(void (^)(NiceResponse *response))callback {
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    NSData *postData = [dataString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    // cookies?!?!?!?!
    NSHTTPCookieStorage *cookieJar = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSDictionary *headers = [NSHTTPCookie requestHeaderFieldsWithCookies: [cookieJar cookies]];
    [request setAllHTTPHeaderFields:headers];
    
    [request setURL:url];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    [request setValue:@"deflate" forHTTPHeaderField:@"Accept-Encoding"];
    [request setValue:@"utf-8" forHTTPHeaderField:@"Accept-Charset"];
    [request setValue:@"ru" forHTTPHeaderField:@"Accept-Language"];
    
    request.HTTPMethod = @"POST";
    request.HTTPBody = postData;
    request.timeoutInterval = TIMEOUT;
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            NiceResponse *niceResponse = [NiceResponse new];
            niceResponse.dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            niceResponse.urlResponse = response;
            niceResponse.statusCode = httpResponse.statusCode;
            
            callback(niceResponse);
        });
    }];
    [postDataTask resume];
}

- (void)DELETERequestWithURL:(NSURL *)url completion:(void (^)(NiceResponse *response))callback {
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    // cookies?!?!?!?!
    NSHTTPCookieStorage *cookieJar = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSDictionary *headers = [NSHTTPCookie requestHeaderFieldsWithCookies: [cookieJar cookies]];
    [request setAllHTTPHeaderFields:headers];
    
    [request setURL:url];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    [request setValue:@"deflate" forHTTPHeaderField:@"Accept-Encoding"];
    [request setValue:@"utf-8" forHTTPHeaderField:@"Accept-Charset"];
    [request setValue:@"ru" forHTTPHeaderField:@"Accept-Language"];
    
    request.HTTPMethod = @"DELETE";
    request.timeoutInterval = TIMEOUT;
    
    NSURLSessionDataTask *deleteDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            NiceResponse *niceResponse = [NiceResponse new];
            niceResponse.dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            niceResponse.urlResponse = response;
            niceResponse.statusCode = httpResponse.statusCode;
            
            callback(niceResponse);
        });
    }];
    [deleteDataTask resume];
}

@end
