//
//  AlamoMine.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 14/03/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

import UIKit
import Alamofire

public typealias JSON = [String : Any]

class AlamoMine: NSObject {
    static func request(_ url: String, method: HTTPMethod, parameters: Parameters?, completion: @escaping (Response?) -> (Void)) {
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
            guard let data = response.data else {
                completion(nil)
                return
            }
            
            let string = String.init(data: data, encoding: .utf8)
            let niceResponse = Response()
            niceResponse.statusCode = response.response?.statusCode
            niceResponse.json = string?.toJSON()
            niceResponse.jsonString = string
            completion(niceResponse)
        }
    }
    
    static func get(_ url: String, completion: @escaping (Response?) -> (Void) ) {
        AlamoMine.request(url, method: .get, parameters: nil) { (response) -> (Void) in
            completion(response)
        }
    }
    
    static func post(_ url: String, json: JSON?, completion: @escaping (Response?) -> (Void) ) {
        let parameters: Parameters? = json
        AlamoMine.request(url, method: .post, parameters: parameters) { (response) -> (Void) in
            completion(response)
        }
    }
    
    static func delete(_ url: String, json: JSON?, completion: @escaping (Response?) -> (Void) ) {
        let parameters: Parameters? = json
        AlamoMine.request(url, method: .delete, parameters: parameters) { (response) -> (Void) in
            completion(response)
        }
    }
}
