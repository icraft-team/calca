//
//  AutoComlpeteView.m
//  DEM
//
//  Created by Valentin Cherepyanko on 30/09/15.
//
//

#import "AutoComlpeteView.h"

@implementation AutoComlpeteView

- (void)awakeFromNib {
    [super awakeFromNib];
    [self hide];
    [self showAnimated];
}

+ (AutoComlpeteView *)initWith:(NSArray *)variants andCoords:(CGPoint)point andDelegate:(NSObject<AutoCompleteViewDelegate> *)delegate w:(CGFloat)width {
    NSUInteger count = [variants count];
    
    CGFloat height = count * 40 + 5;
    if (height > MAX_HEIGHT) height = MAX_HEIGHT;
    
    AutoComlpeteView *view = [AutoComlpeteView new];
    view.frame = CGRectMake(point.x, point.y, width, height);

//    AutoComlpeteView *view = (AutoComlpeteView *)[[AutoComlpeteView alloc] initWithFrame:CGRectMake(point.x, point.y, width, height)];
    
    
    view.delegate = delegate;
    
    view.backgroundColor = [Utils getColorFromInt:MAIN_COLOR];
    view.clipsToBounds = YES;
    view.layer.cornerRadius = 3.0f;
    
    view.scrollView = [[UIScrollView alloc] initWithFrame:view.bounds];
    [view addSubview:view.scrollView];
    
    int i = 0;
    for (NSObject *variant in variants) {
        UIView *subview = [view getViewForDataPiece:variant];
        view.tag = i;
        subview.frame = CGRectMake(5, 40 * i + 5, width - 10, 35);
        [view.scrollView addSubview:subview];
        i++;
    }
    [view.scrollView setContentSize:CGSizeMake(width, count * 40 + 5)];
    
    return view;
}

- (UIView *)getViewForDataPiece:(NSObject *)data {
    ShapedButton *button = [[ShapedButton alloc] init];
    button.cornerRadius = 3;
    button.backgroundColor = [UIColor whiteColor];
    button.tintColor = [Utils getColorFromInt:SECOND_COLOR];
    [button setTitleColor:[Utils getColorFromInt:SECOND_COLOR] forState:UIControlStateHighlighted];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    button.contentEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
    
    button.titleLabel.font = [UIFont fontWithName:@"Open Sans" size:14];
    button.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    [button setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [button setTitle:(NSString *)data forState:UIControlStateNormal];
    [button addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}

+ (AutoComlpeteView *)initWith:(NSArray *)variants andTextField:(UITextField *)textfield andDelegate:(NSObject<AutoCompleteViewDelegate> *)delegate {
    CGFloat x = textfield.frame.origin.x;
    CGFloat y = textfield.frame.origin.y + textfield.frame.size.height + 5;
    
    CGPoint superviewPoint = [textfield.superview convertPoint:CGPointMake(x, y) toView:textfield.superview.superview];
    
    return [AutoComlpeteView initWith:variants andCoords:superviewPoint andDelegate:delegate w:textfield.frame.size.width];
}

- (void)buttonTapped:(UIButton*)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didSelectVariant:)]) {
        [self.delegate didSelectVariant:sender.tag];
    }
    
    [self close];
}

- (void)close {
    [self hideAnimatedWithCompletion:^{
        [self removeFromSuperview];
    }];
}

- (void)didSelectVariant:(NSUInteger)index {
    
}

@end
