//
//  LoadingBlocker.h
//  AutoHelper
//
//  Created by Valentin Cherepyanko on 08/07/16.
//  Copyright © 2016 Valentin Cherepyanko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AnimationHelper.h"
#import "UIView+Animations.h"
#import "AdjustableBlurEffect.h"

@interface LoadingBlocker : UIView

@property (weak, nonatomic) IBOutlet UIImageView *activityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleHeight;

+ (void)showOn:(UIView *)superView light:(BOOL)light;
+ (void)hideFrom:(UIView *)superView;
+ (void)showOn:(UIView *)superView light:(BOOL)light title:(NSString *)title;

@end
