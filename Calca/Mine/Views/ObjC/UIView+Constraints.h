//
//  UIView+Constraints.h
//  DEM
//
//  Created by Valentin Cherepyanko on 31/08/2017.
//
//

#import <UIKit/UIKit.h>

@interface UIView (Constraints)

//depreacted
//- (void)addLeading:(CGFloat)constant to:(UIView *)view;
//- (void)addTrailing:(CGFloat)constant to:(UIView *)view;
//- (void)addTop:(CGFloat)constant to:(UIView *)view;
//- (void)addBottom:(CGFloat)constant to:(UIView *)view;
//- (void)addTopMargin:(CGFloat)constant to:(UIView *)view;



- (NSLayoutConstraint *)addHeight:(CGFloat)constant;
- (NSLayoutConstraint *)addWidth:(CGFloat)constant;

- (NSLayoutConstraint *)setTop:(CGFloat)constant to:(UIView *)view;
- (NSLayoutConstraint *)setLeading:(CGFloat)constant to:(UIView *)view;
- (NSLayoutConstraint *)setTrailing:(CGFloat)constant to:(UIView *)view;
- (NSLayoutConstraint *)setBottom:(CGFloat)constant to:(UIView *)view;

- (void)fitIn:(UIView *)parentView;

- (NSLayoutConstraint *)setVerticalMargin:(CGFloat)constant to:(UIView *)view;

- (NSLayoutConstraint *)constraintForAttribute:(NSLayoutAttribute)attr;
- (NSLayoutConstraint *)setCenterYMargin:(CGFloat)constant to:(UIView *)view;
- (NSLayoutConstraint *)setCenterXMargin:(CGFloat)constant to:(UIView *)view;

- (void)animateHeightConstraintToValue:(CGFloat)constant;

@end
