//
//  AutoComlpeteView.h
//  DEM
//
//  Created by Valentin Cherepyanko on 30/09/15.
//
//

#import <UIKit/UIKit.h>
#import "Utils.h"
#import <QuartzCore/QuartzCore.h>
#import "UIView+Animations.h"
#import "ShapedButton.h"
#import "AutocompleteEntry.h"

#define MAX_HEIGHT 380

@protocol AutoCompleteViewDelegate
- (void)didSelectVariant:(NSInteger)index;
@end

@interface AutoComlpeteView : UIView

@property (weak) NSObject<AutoCompleteViewDelegate> *delegate;
@property UIScrollView *scrollView;

//+ (AutoComlpeteView *)initWith:(NSArray *)variants andCoords:(CGPoint)point andDelegate:(NSObject<AutoCompleteViewDelegate> *)delegate;
+ (AutoComlpeteView *)initWith:(NSArray *)variants andTextField:(UITextField *)textfield andDelegate:(NSObject<AutoCompleteViewDelegate> *)delegate;
- (void)close;
- (UIView *)getViewForDataPiece:(NSObject *)data;

@end
