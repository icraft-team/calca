//
//  UIView+Constraints.m
//  DEM
//
//  Created by Valentin Cherepyanko on 31/08/2017.
//
//

#import "UIView+Constraints.h"

@implementation UIView (Constraints)

- (NSLayoutConstraint *)addHeight:(CGFloat)constant {
    NSLayoutConstraint *constraint = [self createConstraint:NSLayoutAttributeHeight to:nil constant:constant];
    [self addConstraint:constraint];
    return constraint;
}

- (NSLayoutConstraint *)addWidth:(CGFloat)constant {
    NSLayoutConstraint *constraint = [self createConstraint:NSLayoutAttributeWidth to:nil constant:constant];
    [self addConstraint:constraint];
    return constraint;
}

- (void)addTopMargin:(CGFloat)constant to:(UIView *)view {
    NSLayoutConstraint *constraint = [NSLayoutConstraint
                                      constraintWithItem:self
                                      attribute:NSLayoutAttributeTop
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:view
                                      attribute:NSLayoutAttributeBottom
                                      multiplier:1.0f
                                      constant:constant];
    [self.superview addConstraint:constraint];
}

- (NSLayoutConstraint *)createConstraint:(NSLayoutAttribute)attr to:(UIView *)view constant:(CGFloat)constant {
    NSLayoutConstraint *constraint = [NSLayoutConstraint
                                   constraintWithItem:self
                                   attribute:attr
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:view
                                   attribute:attr
                                   multiplier:1.0f
                                   constant:constant];
    return constraint;
}

- (NSLayoutConstraint *)setTop:(CGFloat)constant to:(UIView *)view {
    NSLayoutConstraint *constraint = [self.topAnchor constraintEqualToAnchor:view.topAnchor constant:constant];
    constraint.active = YES;
    return constraint;
}

- (NSLayoutConstraint *)setLeading:(CGFloat)constant to:(UIView *)view {
    UILayoutGuide *margins = view.layoutMarginsGuide;
    NSLayoutConstraint *constraint = [self.leadingAnchor constraintEqualToAnchor:margins.leadingAnchor];
    constraint.constant = constant;
    constraint.active = YES;
    return constraint;
}

- (NSLayoutConstraint *)setTrailing:(CGFloat)constant to:(UIView *)view {
    UILayoutGuide *margins = view.layoutMarginsGuide;
    NSLayoutConstraint *constraint = [self.trailingAnchor constraintEqualToAnchor:margins.trailingAnchor];
    constraint.constant = constant;
    constraint.active = YES;
    return constraint;
}

- (void)fitIn:(UIView *)parentView {
    [self setLeading:0 to:parentView];
    [self setTrailing:0 to:parentView];
    [self setTop:0 to:parentView];
    [self setBottom:0 to:parentView];
}

- (NSLayoutConstraint *)setBottom:(CGFloat)constant to:(UIView *)view {
    NSLayoutConstraint *constraint = [self.bottomAnchor constraintEqualToAnchor:view.bottomAnchor constant:constant];
    constraint.active = YES;
    return constraint;
}

- (NSLayoutConstraint *)setVerticalMargin:(CGFloat)constant to:(UIView *)view {
    NSLayoutConstraint *constraint = [self.topAnchor constraintEqualToAnchor:view.bottomAnchor constant:constant];
    constraint.active = YES;
    return constraint;
}

- (NSLayoutConstraint *)setCenterYMargin:(CGFloat)constant to:(UIView *)view {
    NSLayoutConstraint *constraint = [self.centerYAnchor constraintEqualToAnchor:view.centerYAnchor constant:constant];
    constraint.active = YES;
    return constraint;
}

- (NSLayoutConstraint *)setCenterXMargin:(CGFloat)constant to:(UIView *)view {
    NSLayoutConstraint *constraint = [self.centerXAnchor constraintEqualToAnchor:view.centerXAnchor constant:constant];
    constraint.active = YES;
    return constraint;
}

- (void)animateHeightConstraintToValue:(CGFloat)constant {
    NSLayoutConstraint *height = [self constraintForAttribute:NSLayoutAttributeHeight];
    height.constant = constant;
    [UIView animateWithDuration:0.5 animations:^{
        [self.superview.superview layoutIfNeeded];
    }];
}

- (NSLayoutConstraint *)constraintForAttribute:(NSLayoutAttribute)attr {
    NSArray *constraints = [self constraints];
    NSLayoutConstraint *neededConstraint = nil;
    for (NSLayoutConstraint *constraint in constraints) {
        if (constraint.firstAttribute == attr) {
            neededConstraint = constraint;
        }
    }
    return neededConstraint;
}

@end
