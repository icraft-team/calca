//
//  TriangleView.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 09/03/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

import UIKit

class TriangleView: UIView {
    @IBInspectable var up: Bool = false
    @IBInspectable var left: Bool = false
    @IBInspectable var color: UIColor = .darkGray
    
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        self.backgroundColor = UIColor.clear
//    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        guard let context = UIGraphicsGetCurrentContext() else { return }
        
        
        if left && up {
            drawLeftUpTriangle(context: context, rect: rect)
        } else if left && !up {
            drawLeftDownTriangle(context: context, rect: rect)
        } else if !left && up {
            drawRightUpTriangle(context: context, rect: rect)
        } else if !left && !up {
            drawRightDownTriangle(context: context, rect: rect)
        }
    }
    
    func drawLeftDownTriangle(context: CGContext, rect: CGRect) {
        context.beginPath()
        context.move(to: CGPoint(x: rect.minX, y: rect.maxY))
        context.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY))
        context.addLine(to: CGPoint(x: rect.minX, y: rect.minY))
        context.closePath()
        
        context.setFillColor(color.cgColor)
        context.fillPath()
    }
    
    func drawRightDownTriangle(context: CGContext, rect: CGRect) {
        context.beginPath()
        context.move(to: CGPoint(x: rect.minX, y: rect.maxY))
        context.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY))
        context.addLine(to: CGPoint(x: rect.maxX, y: rect.minY))
        context.closePath()
        
        context.setFillColor(color.cgColor)
        context.fillPath()
    }
    
    func drawLeftUpTriangle(context: CGContext, rect: CGRect) {
        context.beginPath()
        context.move(to: CGPoint(x: rect.minX, y: rect.minY))
        context.addLine(to: CGPoint(x: rect.maxX, y: rect.minY))
        context.addLine(to: CGPoint(x: rect.minX, y: rect.maxY))
        context.closePath()
        
        context.setFillColor(color.cgColor)
        context.fillPath()
    }
    
    func drawRightUpTriangle(context: CGContext, rect: CGRect) {
        context.beginPath()
        context.move(to: CGPoint(x: rect.minX, y: rect.minY))
        context.addLine(to: CGPoint(x: rect.maxX, y: rect.minY))
        context.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY))
        context.closePath()
        
        context.setFillColor(color.cgColor)
        context.fillPath()
    }
}
