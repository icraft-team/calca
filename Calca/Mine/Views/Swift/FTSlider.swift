//
//  FTSlider.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 09/03/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

import UIKit

class FTSlider: UIView {
    @IBOutlet weak var leftTriangle: TriangleView!
    @IBOutlet weak var rightTriangle: TriangleView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var line: UIView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        line.show(self.width > leftTriangle.width)
        leftTriangle.show(self.width > leftTriangle.width)
    }
}
