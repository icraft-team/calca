//
//  AutocompleteCell.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 16/02/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

import UIKit

class AutocompleteCell: UITableViewCell {

    @IBInspectable let tapColor: UIColor = UIColor.lightGray
    @IBInspectable let color: UIColor = UIColor.white
    @IBOutlet weak var panel: NiceView!
    
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()

//        self.layer.drawsAsynchronously = true
        self.selectionStyle = UITableViewCellSelectionStyle.none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        panel.backgroundColor = tapColor
        UIView.animate(withDuration: 0.3) {
            self.panel.backgroundColor = self.color
        }
    }
    
    func configure(with: Any) { }
}
