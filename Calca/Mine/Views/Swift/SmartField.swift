//
//  SmartField.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 27/11/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

import UIKit

protocol SmartFieldDelegate {
    func chosenVariant(variant: String)
}

class SmartField: UITextField {
    @IBInspectable var icon: UIImage?
    @IBInspectable var highlightable: Bool = false
    var sfDelegate: SmartFieldDelegate?
    
    var howToParseData: ((Any) -> (String))?
    var data: Any? {
        didSet {
            if let data = data, let parseDataClosure = howToParseData {
                self.text = parseDataClosure(data)
            }
        }
    }
    
    var variants : [String] = [String]() {
        didSet {
            text = variants[0]
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func setup() {
        addToolBar(textField: self)
        addIcon(textField: self)
        if highlightable { addShadow(radius: 5, opacity: 0.2) }
    }
    
    func addToolBar(textField: UITextField){
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(donePressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        textField.inputAccessoryView = toolBar
    }
    
    func addIcon(textField: UITextField){
        if let icon = self.icon {
            leftViewMode = UITextFieldViewMode.always
            leftView = UIImageView(image:icon)
            leftView?.frame = CGRect(x:0,y:0,width:40,height:40)
        }
    }
    
    @objc func donePressed(){
        self.endEditing(true)
    }
    
    var ac : FieldVariantsView? = nil {
        willSet { ac?.removeFromSuperview() }
    }
    
    var selecting : Bool = false
    override func becomeFirstResponder() -> Bool {
        if variants.count > 0, !selecting {
            selecting = true
            ac = FieldVariantsView(textfield:self, variants:variants, delegate:self)
            superview?.addSubview(ac!)
            return false
        }
        
        if highlightable { self.highlight(true) }
        return super.becomeFirstResponder()
    }
    
    override func resignFirstResponder() -> Bool {
        if highlightable { self.highlight(false) }
        return super.resignFirstResponder()
    }
}

extension SmartField: FieldVariantsDelegate {
    func didSelectVariant(_ index: Int) {
        self.text = variants[index]
        selecting = false
        
        sfDelegate?.chosenVariant(variant: variants[index])
    }
    
    func selectedIndex() -> Int {
        return variants.index(of: text!)!
    }
}

extension SmartField: UIHighlightProtocol {
    func highlight(_ highlighted: Bool) {
//        let shadowRadius: CGFloat = highlighted ? 5 : 0
        let shadowOpacity: Float = highlighted ? 0.2 : 0

        UIView.animate(withDuration: 0.4) {
//            self.layer.shadowRadius = shadowRadius
            self.layer.shadowOpacity = shadowOpacity
        }
    }
}
