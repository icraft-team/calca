//
//  AutocompleteTableView.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 16/02/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

import UIKit

protocol AutocompleteTableViewDelegate: class {
    func selectedRow(index: Int)
    func autocompleteTableDidScroll()
}

class AutocompleteTableView: NiceView {
    static let MAX_HEIGHT : CGFloat = 400
    weak var delegate: AutocompleteTableViewDelegate?
    var tableView: UITableView
    var variants: [Any?] = [Any]() {
        didSet { if variants.count > 0 { self.tableView.reloadData() } }
    }
    
    init() {
        self.tableView = UITableView()
        super.init(frame:CGRect(x:10, y:10, width: 200, height: 100))
        self.backgroundColor = UIColor.clear
        
        self.layer.cornerRadius = 5
        self.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        self.clipsToBounds = true
        self.translatesAutoresizingMaskIntoConstraints = false
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = UIColor.clear
        tableView.separatorStyle = .none
        tableView.clipsToBounds = false
        self.addSubview(tableView)
        _ = tableView.setLeading(-8, to: self)
        _ = tableView.setTrailing(8, to: self)
        _ = tableView.setTop(0, to: self)
        _ = tableView.setBottom(0, to: self)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 50
        tableView.rowHeight = UITableViewAutomaticDimension
        self.registerCell(tableView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        variants = [Any]()
        tableView = UITableView()
        super.init(coder: aDecoder)
    }
    
    // override in child for using another
    func registerCell(_ tableView : UITableView) {
        tableView.register(UINib(nibName: "AutocompleteCell", bundle: Bundle.main), forCellReuseIdentifier: "AutocompleteCell")
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        delegate?.autocompleteTableDidScroll()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.selectedRow(index: indexPath.row)
        self.fade { self.removeFromSuperview() }
//        closeAnimatingCell(indexPath)
    }
    
    func closeAnimatingCell(_ indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath), tableView.visibleCells.contains(cell) {
            cell.zoomfade { self.removeFromSuperview() }
            for otherCell in tableView.visibleCells {
                if otherCell != cell {
                    otherCell.fade()
                }
            }
        }
    }
}


typealias AutocompleteTableDelegate = AutocompleteTableView
extension AutocompleteTableDelegate: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return variants.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return getCellFor(indexPath)
    }
    
    @objc func getCellFor(_ indexPath : IndexPath) -> AutocompleteCell {
        var cell : AutocompleteCell? = tableView.dequeueReusableCell(withIdentifier: "AutocompleteCell") as! AutocompleteCell?
        
        if (cell == nil) {
            cell = AutocompleteCell(style: .subtitle, reuseIdentifier: "AutocompleteCell")
        }
        
        return cell!
    }
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        let autocompleteCell = cell as! AutocompleteCell
//        if let variant = variants[indexPath.row] {
//            autocompleteCell.configure(with: variant)
//        }
//    }
}
