//
//  DropdownButton.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 13/03/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

import UIKit

class DropdownButton: ShapedButton {
    @IBOutlet weak var dropdownIcon: UIImageView!
    
    var dropped: Bool = false {
        didSet {
            if dropped, dropped != oldValue {
                AnimationHelper.rotateUP(dropdownIcon, duration: 0)
            } else if !dropped, dropped != oldValue {
                AnimationHelper.rotateDown(dropdownIcon, duration: 0)
            }
        }
    }
    var active: Bool = false {
        didSet {
            self.dropdownIcon.show(active)
        }
    }
    
    override func didTouch() {
        super.didTouch()

        if !active { return }
        dropped = !dropped
    }
    
//    func drop(_ drop: Bool) {
//        dropped = drop
//    }
    
    func activate(_ active: Bool) {
        self.active = active
        if !active, dropped {
            dropdownIcon.transform = dropdownIcon.transform.rotated(by: CGFloat.pi)
        }
    }
}
