//
//  NiceView.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 01/05/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

import UIKit

class NiceView: UIView {

    @IBInspectable var cornerRadius : CGFloat = 0
    @IBInspectable var color : UIColor = .white
    
    @IBInspectable var shadowOpacity : Float = 1
    @IBInspectable var shadowRadius : CGFloat = 1
    @IBInspectable var shadowColor : CGColor = UIColor.black.cgColor
    
    @IBInspectable var shadowOffsetX : Int = 1
    @IBInspectable var shadowOffsetY : Int = 1
    
    
    override func awakeFromNib() {
        setup()
    }
    
    func setup() {
        backgroundColor = color
        layer.cornerRadius = cornerRadius
        layer.shadowColor = shadowColor
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
        layer.shadowOffset = CGSize(width: shadowOffsetX, height: shadowOffsetY)
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
