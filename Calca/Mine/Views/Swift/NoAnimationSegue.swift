//
//  NoAnimationSegue.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 12/02/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

import UIKit

class NoAnimationSegue: UIStoryboardSegue {
    override func perform() {
        self.source.navigationController?.pushViewController(self.destination, animated: false)
    }
}
