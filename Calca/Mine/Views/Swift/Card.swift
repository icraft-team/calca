//
//  Card.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 16/01/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

import UIKit
import pop

protocol CardDelegate: class {
    func finished(card: Card)
}

class Card: NiceView {
    
    weak var delegate : CardDelegate?
    var index : Int = 0
    
    func showWithPopup() {
        self.pop_add(AnimationHelper.popupAnimation(), forKey: "popup")
    }
    
    func showWithUnfade() {
        hide()
        showAnimated()
        self.pop_add(AnimationHelper.unfadeAnimation(), forKey: "unfade")
    }
    
    override func awakeFromNib() {
        self.clipsToBounds = false
        super.awakeFromNib()
    }
    
    func configureWith(object: Any) {
        
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
