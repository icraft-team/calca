//
//  AutocompleteView.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 01/02/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

import UIKit

protocol FieldVariantsDelegate: class {
    func didSelectVariant(_ index : Int)
}


class FieldVariantsView: UIView {
    static let MAX_ROWS = 10
    let rowHeight : CGFloat = 34
    weak var delegate : FieldVariantsDelegate?
    
    // you need add this view to superview of passable text field
    init(textfield : UITextField, variants : [String], delegate : FieldVariantsDelegate) {
        let x = textfield.frame.origin.x
        let y = textfield.frame.origin.y + textfield.frame.size.height + 4
        
        let width = textfield.frame.size.width
        let rowsCount = variants.count <= FieldVariantsView.MAX_ROWS ? variants.count : FieldVariantsView.MAX_ROWS
        let height = rowHeight * CGFloat(rowsCount)
        
        super.init(frame: CGRect(x:x, y:y, width:width, height:height))
        
        self.delegate = delegate
        
        clipsToBounds = false
        layer.cornerRadius = 5
        backgroundColor = UIColor.black.withAlphaComponent(0.3)
        
        var i : Int = 0
        for variant in variants {
            let button = createButton(variant: variant, index: i)
            self.addSubview(button)
            i += 1
            if i > FieldVariantsView.MAX_ROWS { break }
        }
    }
    
    func createButton(variant: String, index: Int) -> ShapedButton {
        let origin = CGPoint(x:2, y:CGFloat(index) * rowHeight + 2)
        let size = CGSize(width: self.frame.width - 4, height: CGFloat(rowHeight) - 4)
        let button = ShapedButton(frame: CGRect(origin:origin, size:size))
        button.color = UIColor.white
        button.tapColor = UIColor.lightGray
        button.backgroundColor = UIColor.white
        button.setTitleColor(UIColor.darkGray, for: UIControlState.normal)
        button.layer.cornerRadius = 3
        button.titleLabel?.font = UIFont(name: "Open Sans", size: 15)
        button.titleLabel?.numberOfLines = 0
        button.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
        button.contentHorizontalAlignment = .left
        button.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0)
        button.setTitle(variant, for: UIControlState.normal)
        button.tag = index
        button.addTarget(self, action: #selector(buttonTapped(sender:)), for:.touchUpInside)
        return button
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @objc func buttonTapped(sender : UIButton) {
        if let d = delegate {
            d.didSelectVariant(sender.tag)
        }
        close()
    }
    
    func close() {
        self.hideAnimated {
            self.removeFromSuperview()
        }
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
