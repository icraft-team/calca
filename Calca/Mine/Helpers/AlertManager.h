//
//  AlertManager.h
//  AutoHelper
//
//  Created by Valentin Cherepyanko on 16/07/16.
//  Copyright © 2016 Valentin Cherepyanko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define LOGIN_INVALID @"login_invalid"
#define FILL_TIME @"fillTime"
#define LOGIN_TAKEN @"login_taken"
#define EMAIL_TAKEN @"email_taken"
#define PROJECT_EXISTS @"project_exists"
#define PROJECT_DOES_NOT_EXIST @"project_dont_exists"

@interface AlertManager : NSObject

+ (AlertManager *)get;
- (UIViewController *)alertForStatus:(NSString *)status;
- (UIAlertController *)alertWithOptions:(NSArray *)actions;

- (void)show:(NSString *)message in:(UIViewController *)controller;

@property NSMutableDictionary *messages;

@end
