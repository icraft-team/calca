//
//  AlertManager.m
//  AutoHelper
//
//  Created by Valentin Cherepyanko on 16/07/16.
//  Copyright © 2016 Valentin Cherepyanko. All rights reserved.
//

#import "AlertManager.h"

@implementation AlertManager

+ (AlertManager *)get {
    static AlertManager *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
        [instance setup];
    });
    return instance;
}

- (void)setup {
    self.messages = [NSMutableDictionary new];
//    [self.messages setObject:@"Этот логин уже занят" forKey:CONFLICT_STATUS];
    //    [self.messages setObject:@"Неверный логин или пароль" forKey:UNAUTHORIZED_STATUS];
    [self.messages setObject:@"Неверный логин" forKey:LOGIN_INVALID];
    [self.messages setObject:@"Укажите затраченное время" forKey:FILL_TIME];
    [self.messages setObject:@"This login already taken" forKey:LOGIN_TAKEN];
    [self.messages setObject:@"This email already taken" forKey:EMAIL_TAKEN];
    [self.messages setObject:@"Этот проект уже существует" forKey:PROJECT_EXISTS];
    [self.messages setObject:@"Такого проекта нет" forKey:PROJECT_DOES_NOT_EXIST];
}

- (UIViewController *)alertForStatus:(NSString *)status {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[self.messages objectForKey:status]
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    //We add buttons to the alert controller by creating UIAlertActions:
    UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok"
                                                       style:UIAlertActionStyleDefault
                                                     handler:nil]; //You can use a block here to handle a press on this button
    [alertController addAction:actionOk];
    return alertController;
}

- (UIAlertController *)alertWithOptions:(NSArray *)actions {
    UIAlertController* alert = [UIAlertController
                                alertControllerWithTitle:nil
                                message:nil
                                preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (UIAlertAction *action in actions) {
        [alert addAction:action];
    }
    return alert;
}

- (void)show:(NSString *)message in:(UIViewController *)controller {
    UIViewController *alert = [self alertForStatus:message];
    [controller presentViewController:alert animated:YES completion:nil];
}

@end
