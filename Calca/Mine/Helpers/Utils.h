//
//  Utils.h
//  DEM
//
//  Created by Valentin Cherepyanko on 16/12/14.
//
//

#import <Foundation/Foundation.h>
#import "ImageProcessor.h"
#import <QuartzCore/QuartzCore.h>

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

#define MAIN_COLOR 0xf72336
#define SECOND_COLOR 0xfcbd26
#define THIRD_COLOR 0x186294

#define MEALS_COLOR 0x33b9fd
#define EXERCISE_COLOR 0x3ad8aa
#define OVER_LIMIT_COLOR 0xe4644b

#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]

@interface Utils : NSObject

+ (UIColor *)getColorFromString:(NSString *)string;
+ (UIColor *)getColorFromInt:(int)number;
+ (void)addShadowToView:(UIView *)view;
+ (void)setNewFrameFor:(UIView *)view withSize:(CGSize)size;
+ (NSString *)removeHTMLfromString:(NSString *)string;
+ (UIViewController*)topController;
+ (UIView*)topView;
+ (CGSize)screenSize;

+ (UIColor *)colorFromHexString:(NSString *)hexString;

+ (UILabel *)createLabelWithText:(NSString *)text
                               x:(CGFloat)x
                               y:(CGFloat)y
                       textColor:(int)color
                        fontSize:(CGFloat)fontSize
                           width:(CGFloat)width;


+ (NSString *)getFormattedDateS:(NSString *)dateString withFormat:(NSString *)format;
+ (NSDate *)getDateFromString:(NSString *)string withFormat:(NSString *)format;
+ (NSString *)getFormattedDate:(NSDate *)date;
+ (NSString *)getFormattedTimeString:(NSDate *)time;
+ (NSString *)getStringFromDate:(NSDate *)date withFormat:(NSString *)format;

+ (UIColor *)randomColor;
+ (NSArray *)getUniqueArray:(NSArray *)array;
+ (UILabel *)rotateLabel:(UILabel *)label rotation:(CGFloat)transformNumber;
+ (UIImage *)imageFromLabel:(UILabel *)label;

+ (void)printFonts;

+ (UIImage *)takeScreenshot;

+ (BOOL)validateEmail:(NSString *)email;
+ (double)roundedDoubleFromString:(NSString *)string;
+ (NSString *)formatNumberString:(NSString*)originalString;
+ (NSNumber *)numberFromString:(NSString *)string;
+ (int)round:(int)number to:(int)rounder;

+ (NSString *)generateUUID;

// project specific
+ (NSString *)getChangedDateWithDate:(NSDate *)date;
+ (NSString *)hoursAndMinutesFromMinutes:(long)minutes;

+ (float)getHeightForLabel:(UILabel *)label;
+ (NSString *)stringFromDates:(NSDate *)date1 second:(NSDate *)date2;
+ (NSString *)stringFromDate:(NSDate *)date withFormat:(NSString *)format;

+ (void)blurView:(UIView *)view;
+ (void)unblurView:(UIView *)view;
+ (BOOL)date:(NSDate *)date isBetweenDate:(NSDate *)beginDate andDate:(NSDate *)endDate;

@end
