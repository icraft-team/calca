//
//  ImageProcessor.h
//  ImageProcessor
//
//  Created by Valentin Cherepyanko on 24/11/14.
//  Copyright (c) 2014 Valentin Cherepyanko. All rights reserved.
//

#define MAX_THUMBNAIL_SIZE 200
#define MAX_IMAGE_SIZE 1000

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreImage/CoreImage.h>

@interface ImageProcessor : NSObject
+ (NSData *)getDataFromImage:(UIImage *)image;
+ (UIImage *)getImageFromData:(NSData *)data;

+ (UIImage *)rotateImage:(UIImage *)image;
+ (UIImage *)invertColors:(UIImage *)image;
+ (UIImage *)mirrorImage:(UIImage *)image;

+ (UIImage *)compressImage:(UIImage *)image;
+ (UIImage *)getResizedImage:(UIImage *)image forSize:(int)size;

+ (UIImage *)imageWithName:(NSString *)name andColor:(UIColor *)color;
+ (UIImage *)imageWithName:(NSString *)name andColor:(UIColor *)color andSize:(int)size;

+ (UIImage*)imageSaturated:(UIImage *)image value:(CGFloat)value;
+ (UIImage *)resizeImage:(UIImage*)image newSize:(CGSize)newSize;
@end
