//
//  RuntimeStorage.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 12/01/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

import UIKit


let SELECTED_DATE_CHANGED = "selected date changed"
class CalcaSettings: NSObject {
    static let shared = CalcaSettings()
    
    var selectedCalendarDate : Date = Date() {
        didSet {
            NotificationCenter.post(SELECTED_DATE_CHANGED)
        }
    }
}
