//
//  AuthManager.swift
//  KingMFC
//
//  Created by Valentin Cherepyanko on 19/04/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

import UIKit

let PROFILE_UPDATE_EVENT = "profile update event"

class AuthManager: NSObject {
    static let get = AuthManager()
    var authorized = false
    var profile : Profile? {
        didSet {
            NotificationCenter.post(PROFILE_UPDATE_EVENT)
        }
    }
    
    func tryRestoreSession(completion: @escaping (Bool) -> Void) {
        guard let authToken: String = Cache.get().objects.object(forKey: AUTH_TOKEN_KEY) as? String else { completion(false); return }
        
        log("trying to restore session with token: " + authToken)
        let request = RestoreAuthRequest(authToken)
        AlamoMine.post(REST.restoreAuth, json: request.toJSON()) { (response) -> (Void) in
            if (response?.statusCode == 200) {
                self.log("successfully restored session with token: " + authToken)
                if let json = response?.json {
                    self.authorized = true
                    self.profile = Profile(json: json)
                    
                    completion(true)
                } else {
                    self.log("restoring session: something wrong with json")
                    completion(false)
                }
            } else {
                self.log("failed to restore session with auth token")
                completion(false)
            }
        }
    }
    
    func authorize(login: String, pass: String, completion: @escaping (Response?) -> Void) {
        let request = AuthRequest(login: login, pass: pass)
        AlamoMine.post(REST.auth, json: request.toJSON()) { (response) -> (Void) in
            if (response?.statusCode == 200) {
                if let json = response?.json {
                    self.authorized = true
                    self.profile = Profile(json: json)
                }
            }
            completion(response)
        }
    }
    
    func logout() {
        Cache.get().objects.removeObject(forKey: AUTH_TOKEN_KEY)
        authorized = false
        profile = nil
    }
    
    func register(login: String,
                  pass: String,
                  email: String,
                  completion: @escaping (Response?) -> Void) {
        
        let timezoneOffset = NSTimeZone.local.secondsFromGMT()
        let request = RegistrationRequest(login: login, pass: pass, email: email, timezoneOffset: timezoneOffset)
        AlamoMine.post(REST.registration, json: request.toJSON()) { (response) -> (Void) in
            if let json = response?.json {
                self.authorized = true
                self.profile = Profile(json: json)
            }
            completion(response)
        }
    }
}
