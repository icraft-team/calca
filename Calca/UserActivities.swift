//
//  UserActivitiesResponse.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 04/03/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

import UIKit
import Gloss

struct UserActivities: Gloss.Decodable {
    var meals: [Meal]?
    var exercises: [PerformedExercise]?
    
    init?(json: JSON) {
        self.meals = "meals" <~~ json
        self.exercises = "exercises" <~~ json
    }
    
    static func fetch(date: Date, completion: @escaping (UserActivities?) -> (Void)) {
        let startOfDay = date.startOfDay.timeIntervalSince1970.toInt
        let endOfDay = date.endOfDay.timeIntervalSince1970.toInt
        let request = UserActivitiesRequest(startOfDay, endOfDay)
        AlamoMine.post(REST.userActivities, json: request.toJSON()) { (response) -> (Void) in
            guard let json = response?.json else { print("[UserActivities] can't parse json into activities"); return }
            let userActivities = UserActivities(json: json)
            completion(userActivities)
        }
    }
    
    static func fetchBetween(_ start: Date, and end: Date, completion: @escaping (UserActivities?) -> (Void)) {
        let startOfDay = start.startOfDay.timeIntervalSince1970.toInt
        let endOfDay = end.endOfDay.timeIntervalSince1970.toInt
        let request = UserActivitiesRequest(startOfDay, endOfDay)
        AlamoMine.post(REST.userActivities, json: request.toJSON()) { (response) -> (Void) in
            guard let json = response?.json else { print("[UserActivities] json is nil"); return }
            let userActivities = UserActivities(json: json)
            completion(userActivities)
        }
    }
    
    func mealSum(_ start: Date,_ end: Date) -> Int {
        let startSeconds = Int(start.timeIntervalSince1970)
        let endSeconds = Int(end.timeIntervalSince1970)
        let mealsSum = meals?.filter({ (meal) -> Bool in
            guard let mealTime = meal.time else {
                print("[UserActivities] some meals don't have meal time, this is wrong")
                return false
            }
            return mealTime >= startSeconds && mealTime < endSeconds
        }).reduce(0, { $0 + Int($1.totalCals) })
        
        if let sum = mealsSum {
            return sum
        } else {
            print("[UserActivities] meals sum in unable to count, please check")
            return 0
        }
    }
    
    func exerciseSum(_ start: Date, _ end: Date) -> Double {
        let startSeconds = Int(start.timeIntervalSince1970)
        let endSeconds = Int(end.timeIntervalSince1970)
        let exerciseSum = exercises?.filter({ (exercise) -> Bool in
            guard let exerciseTime = exercise.time else {
                print("[UserActivities] some exercises don't have meal time, this is wrong")
                return false
            }
            return exerciseTime >= startSeconds && exerciseTime < endSeconds
        }).reduce(0, { $0 + Double($1.caloriesBurnt) })
        
        if let sum = exerciseSum {
            return sum
        } else {
            print("[UserActivities] exercise sum in unable to count, please check")
            return 0
        }
    }
}
