//
//  MealTableDelegate.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 09/05/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

import UIKit


class MealTableDelegate: NSObject, UITableViewDelegate, UITableViewDataSource {
    static let rowHeight : CGFloat = 50
    
    var tableView : UITableView? {
        didSet {
            tableView?.delegate = self
            tableView?.dataSource = self
            tableView?.estimatedRowHeight = 50
            tableView?.rowHeight = UITableViewAutomaticDimension
        }
    }
    
    var meals : [Meal]? {
        didSet {
            tableView?.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let mealsCount = meals?.count {
            return mealsCount
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : MealCell? = tableView.dequeueReusableCell(withIdentifier: "MealCell") as! MealCell?
        
        if (cell == nil) {
            cell = MealCell(style: .subtitle, reuseIdentifier: "MealCell")
        }
        
        if let meal = meals?[indexPath.row] {
            cell?.configure(with: meal)
        }
        
        return cell!
    }
    
    var contentHeight: CGFloat {
        var total: CGFloat = 0
        guard let meals = self.meals else { return 0 }
        
        for meal in meals {
            guard let text = meal.dishName else { log("kek! look at it, NOW"); return 0 }
            total += MealCell.heightFor(text)
        }
        return ceil(total)
    }
    
    func delete(_ meal: Meal) {
        guard let index = meals?.index(of: meal) else { return }
        let indexPaths = [IndexPath(row:index, section: 0)]
        tableView?.beginUpdates()
        tableView?.deleteRows(at: indexPaths, with: UITableViewRowAnimation.fade)
        meals?.remove(object: meal)
        tableView?.endUpdates()
    }
    
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
//        if editingStyle == UITableViewCellEditingStyle.delete {
//
//        }
//    }
}
