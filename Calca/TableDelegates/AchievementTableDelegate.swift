//
//  AchievementTableDelegate.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 27/03/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

import UIKit

class AchievementTableDelegate: NSObject, UITableViewDelegate, UITableViewDataSource {
    var tableView : UITableView? {
        didSet {
            tableView?.delegate = self
            tableView?.dataSource = self
            tableView?.estimatedRowHeight = 50
            tableView?.rowHeight = UITableViewAutomaticDimension
        }
    }
    
    var achievements: [Achievement] = [Achievement]() {
        didSet {
            tableView?.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return achievements.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : AchievementCell? = tableView.dequeueReusableCell(withIdentifier: "AchievementCell") as! AchievementCell?
        
        if (cell == nil) {
            cell = AchievementCell(style: .subtitle, reuseIdentifier: "AchievementCell")
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? AchievementCell {
            cell.configure(with: achievements[indexPath.row])
        }
    }
}
