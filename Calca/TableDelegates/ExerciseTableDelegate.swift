//
//  ExerciseTableDelegate.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 06/03/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

import UIKit

class ExerciseTableDelegate: NSObject, UITableViewDelegate, UITableViewDataSource {
    static let rowHeight : CGFloat = 50
    
    var tableView : UITableView? {
        didSet {
            tableView?.delegate = self
            tableView?.dataSource = self
            tableView?.estimatedRowHeight = 50
            tableView?.rowHeight = UITableViewAutomaticDimension
        }
    }
    
    var exercises : [PerformedExercise]? {
        didSet {
            tableView?.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let rowsCount = exercises?.count else {
            return 0
        }
        
        return rowsCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : PerformedExerciseCell? = tableView.dequeueReusableCell(withIdentifier: "ExerciseCell") as! PerformedExerciseCell?
        
        if (cell == nil) {
            cell = PerformedExerciseCell(style: .subtitle, reuseIdentifier: "MealCell")
        }
        
        if let exercise = exercises?[indexPath.row] {
            cell?.configure(with: exercise)
        }
        
        return cell!
    }
    
    func delete(exercise: PerformedExercise) {
        guard let index = exercises?.index(of: exercise) else { return }
        let indexPaths = [IndexPath(row:index, section: 0)]
        tableView?.beginUpdates()
        tableView?.deleteRows(at: indexPaths, with: UITableViewRowAnimation.fade)
        exercises?.remove(at: index)
        tableView?.endUpdates()
    }
    
    var contentHeight: CGFloat {
        var total: CGFloat = 0
        guard let exercises = self.exercises else { return 0 }
        
        for exercise in exercises {
            guard let text = exercise.name else { log("kek! look at it, NOW"); return 0 }
            total += PerformedExerciseCell.heightFor(text)
        }
        return ceil(total)
    }
}
