//
//  AddMealController.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 01/05/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

import UIKit

let ADD_MEAL_EVENT = "add meal event"

class AddMealController: NiceViewController, AutoCompleteViewDelegate, DishAutocompleteDelegate {
    @IBOutlet weak var formHeight: NSLayoutConstraint!
    @IBOutlet weak var mealTextfield: UITextField!
    @IBOutlet weak var createDish: ModalView!
    @IBOutlet weak var calsPerLabel: UILabel!
    @IBOutlet weak var form: UIView!
    @IBOutlet weak var gramsOrNumberField: UITextField!
    @IBOutlet weak var dateView: DateView!
    
    @IBOutlet weak var addMeal: ModalView!
    @IBOutlet weak var singleSwitch: UISwitch!
    @IBOutlet weak var calsPerField: UITextField!
    @IBOutlet weak var newDishNameField: UITextField!
    @IBOutlet weak var errorLabelHeight: NSLayoutConstraint!
    
    @IBOutlet weak var mealFieldActivityIndicator: UIActivityIndicatorView!
    
    var mealSearchTimer : Timer?
    
    var dishes : [Dish] = [Dish]()
    var selectedDish : Dish? {
        didSet {
            setupForm()
        }
    }
    
    var dishAC : DishAutocomplete?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setup()
    }
    
    func setup() {
        dateView.select(date: CalcaSettings.shared.selectedCalendarDate)
        
        calsPerLabel.hide()
        form.hide()
        
        createDish.hide()
        addMeal.hide()
        mealTextfield.becomeFirstResponder()
        addMeal.unfade()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mealTextfield.addTarget(self, action: #selector(mealTextfieldChanged(_:)), for: .editingChanged)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func mealTextfieldChanged(_ textField: UITextField) {
        self.dishAC?.close()
        shrinkForm()
        if (mealSearchTimer != nil) { mealSearchTimer?.invalidate() }
        proceedMealSearch()
    }
    
    func proceedMealSearch() {
        mealFieldActivityIndicator.startAnimating()
        if mealTextfield.text == "" { return }
        guard let searchString = mealTextfield.text else { return }
        
        AlamoMine.get(REST.mealAutocomplete(searchString)) { (response) -> (Void) in
            self.mealFieldActivityIndicator.stopAnimating()
            if let jsonArray = response?.jsonString?.toJSONArray() {
                guard let dishesArray = [Dish].from(jsonArray: jsonArray) else {
                    self.log("can't parse json to dishes")
                    return
                }
                
                self.dishes = dishesArray
                self.dishAC = DishAutocomplete.getWith(self.dishes, self)
                self.dishAC?.addTo(view: self.view, anchor: self.mealTextfield)
            }
        }
    }
    
    func didSelectVariant(_ index: Int) {
        expandForm()
        
        self.selectedDish = dishes[index]
        self.mealTextfield.text = self.selectedDish?.name
        self.gramsOrNumberField.becomeFirstResponder()
    }

    func expandForm() {
        calsPerLabel.showAnimated()
        self.formHeight.setValueAnimated(96, self.view)
        
        form.showAnimated()
    }
    
    func shrinkForm() {
        calsPerLabel.hideAnimated()
        self.formHeight.setValueAnimated(0)
        form.hideAnimated()
    }
    
    override func close(_ sender: Any) {
        addMeal.fade()
        createDish.fade()
        
        super.close(sender)
        self.mealTextfield.resignFirstResponder()
    }
    
    func setupForm() {
        let singled = selectedDish?.singled ?? false
        
        if let calories = selectedDish?.calsPer {
            let stringValue = String(describing: calories)
            if singled {
                calsPerLabel.text = stringValue + "kcal/serving"
                gramsOrNumberField.placeholder = "Number of servings"
            } else {
                gramsOrNumberField.placeholder = "Portion weight"
                calsPerLabel.text = stringValue + "kcal/100g"
            }
        }
    }
    
    @IBAction func createNewDish(_ sender: Any) {
        if !checkFields() { return }
        
        let request = CreateDishRequest(name: newDishNameField.text!, calsPer: Int(calsPerField.text!)!, singled: singleSwitch.isOn)
        
        LoadingBlocker.show(on: self.createDish, light: true)
        
        
        
        post(REST.createDish, request.toJSON()) { (response) in
            LoadingBlocker.hide(from: self.createDish)
            if (response?.statusCode == 200) {
                self.showAddMealWindow(self)
                self.errorLabelHeight.setValueAnimated(0, self.addMeal)
            } else if (response?.statusCode == 409) {
                self.errorLabelHeight.setValueAnimated(18, self.addMeal)
                //                AlertManager.get().show("This meal name already taken, try something different", in: self)
            }
        }
    }
    
    @IBAction func addMealToDay(_ sender: Any) {
        if (!checkFields()) { return }
        
        let request = AddMealRequest(selectedDish!, Int(gramsOrNumberField.text!)!, CalcaSettings.shared.selectedCalendarDate)
        
        LoadingBlocker.show(on: self.addMeal, light: true)
        post(REST.addMeal, request.toJSON()) { (response) in
            LoadingBlocker.hide(from: self.addMeal)
            if response?.statusCode == 200 {
                if let json = response?.json {
                    AuthManager.get.profile = Profile(json: json)
                    self.gramsOrNumberField.resignFirstResponder()
                    NotificationCenter.post(ADD_MEAL_EVENT)
                    self.close()
                }
            }
        }
    }
    
    
    @IBAction func showCreateDishWindow(_ sender: Any) {
        self.dishAC?.close()
        createDish.unfade()
        addMeal.fade()
        newDishNameField.becomeFirstResponder()
    }
    
    @IBAction func showAddMealWindow(_ sender: Any) {
        mealTextfield.becomeFirstResponder()
        createDish.fade()
        addMeal.unfade()
    }
    
    @IBAction func singeldSwitched(_ sender: Any) {
        if singleSwitch.isOn {
            calsPerField.placeholder = "Calories/Serving"
        } else {
            calsPerField.placeholder = "Calories/100g"
        }
    }
}

extension AddMealController: DateViewDelegate {
    func dateSelected(_ date : Date) {
        CalcaSettings.shared.selectedCalendarDate = date
    }
}
