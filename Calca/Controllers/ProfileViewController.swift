//
//  ProfileViewController.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 26/11/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

import UIKit

class ProfileViewController: UITableViewController {
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var caloriesLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var avatarUnderlay: UIView!
    @IBOutlet weak var weightView: UIView!
    @IBOutlet weak var weightMetricLabel: UILabel!
    @IBOutlet weak var unitSystemSwitch: UISegmentedControl!
    
    @IBOutlet weak var weightInputView: UIView!
    @IBOutlet weak var weightInput: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        showProfileInfo()
        self.subscribe(PROFILE_UPDATE_EVENT, #selector(showProfileInfo))
    }
    
    func setup() {
        weightInputView.hide()
        
        avatarUnderlay.layer.cornerRadius = 65
        avatarUnderlay.layer.shadowColor = UIColor.black.cgColor;
        avatarUnderlay.layer.shadowOpacity = 0.2;
        avatarUnderlay.layer.shadowRadius = 1;
        avatarUnderlay.layer.shadowOffset = CGSize(width: 1, height: 1)
        
        avatar.layer.cornerRadius = 60
        avatar.clipsToBounds = true
        tableView.tableFooterView = UIView.init(frame: CGRect.zero)
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        NotificationCenter.default.addObserver(self, selector: #selector(ProfileViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//    }
    
//    func keyboardWillShow(_ notification: Notification) {
//        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
//            let keyboardRectangle = keyboardFrame.cgRectValue
//            let keyboardHeight = keyboardRectangle.height
//            let scrollTo = Utils.screenSize().height - keyboardHeight
//            tableView.setContentOffset(CGPoint(x: 0, y: scrollTo), animated: true)
//        }
//    }
    
    @objc func showProfileInfo() {
        if let profile = AuthManager.get.profile {
            // setting label
            loginLabel.text = profile.user?.login
            
            // serring daily intake calories
            if let dailyIntake = profile.user?.caloriesPerDay {
                caloriesLabel.text = String(dailyIntake) + "kcal"
            }
            
            // setting weight
            if let weight = profile.user?.weight {
                weightLabel.text = weight.toString + units
            } else {
                weightLabel.text = "0"
            }
            
            
            // setting unit system
            if let metric = profile.user?.metricSystem {
                let units = metric ? 1 : 0
                unitSystemSwitch.selectedSegmentIndex = units
            }
            
            // setting email
            emailLabel.text = profile.user?.email
        }
    }
    
    @IBAction func editWeightTapped(_ sender: Any) {
        weightView.hide()
        weightInputView.show()
        weightMetricLabel.text = units
        if let weight = AuthManager.get.profile?.user?.weight {
            weightInput.text = weight.toString
        }
        weightInput.becomeFirstResponder()
    }
    
    @IBAction func doneEditWeightTapped(_ sender: Any) {
        if let weightString = weightInput.text {
            AuthManager.get.profile?.user?.weight = Float(weightString)
            AuthManager.get.profile?.user?.sync()
            weightLabel.text = weightString + units
        }
        
        weightView.show()
        weightInputView.hide()
        weightInput.resignFirstResponder()
    }
    
    @IBAction func editCaloriesTapped(_ sender: Any) {
        self.performSegue(withIdentifier: Cards, sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Cards {
            let CardsController = segue.destination as! CardsViewController
            CardsController.passData([CaloriesCard])
        }
    }
    
    @IBAction func logoutButtonTapped(_ sender: Any) {
        AuthManager.get.logout()
        self.performSegue(withIdentifier: "AuthFromProfile", sender: self)
    }
    
    @IBAction func unitSystemSwitched(_ sender: Any) {
        AuthManager.get.profile?.user?.metricSystem = unitSystemSwitch.selectedSegmentIndex == 1
        AuthManager.get.profile?.user?.sync()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 34
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect.init(x: 0, y: 0, width: 200, height: 34))
        let label = UILabel.init(frame: CGRect.init(x: 16, y: 0, width: 200, height: 34))
        label.font = UIFont(name: "Open Sans", size: 16)
        if section == 1 {
            label.text = "Personal information"
        } else if section == 2 {
            label.text = "Application settings"
        }
        view.addSubview(label)
        
        return view
    }
}

extension ProfileViewController {
    var units: String {
        var units = " lb"
        if let metricSystem = AuthManager.get.profile?.user?.metricSystem, metricSystem {
            units = " kg"
        }
        return units
    }
}
