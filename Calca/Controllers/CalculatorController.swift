//
//  ViewController.swift
//  Calca (old name)
//
//  Created by Valentin Cherepyanko on 26/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

import UIKit
import CircleProgressBar
import FSCalendar

class CalculatorController: NiceViewController {
    @IBOutlet weak var mealsTableView: UITableView!
    @IBOutlet weak var mealsTableHeight: NSLayoutConstraint!
    @IBOutlet weak var exerciseTableView: UITableView!
    @IBOutlet weak var exerciseTableHeight: NSLayoutConstraint!
    
    @IBOutlet weak var metricsView: MetricsView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var dateView: DateView!
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var mealsButton: DropdownButton!
    @IBOutlet weak var exerciseButton: DropdownButton!
    
    var mealsTableDelegate: MealTableDelegate = MealTableDelegate()
    var exercisesTableDelegate: ExerciseTableDelegate = ExerciseTableDelegate()
    
    var userActivities: UserActivities? {
        didSet {
            mealsTableDelegate.meals = userActivities?.meals
            exercisesTableDelegate.exercises = userActivities?.exercises
            
            updateButtons()
            updateMealsTableView()
            updateExercisesTableView()
            
            guard let userActivities = self.userActivities else { return }
            metricsView.configure(userActivities: userActivities, caloriesLimit: AuthManager.get.profile?.user?.caloriesPerDay)
        }
    }
    
    deinit { unsubscribe() }
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        subscribeToEvents()
        
        guard let userActivities = self.userActivities else { return }
        metricsView.configure(userActivities: userActivities, caloriesLimit: AuthManager.get.profile?.user?.caloriesPerDay)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.unsubscribe()
    }
    
    func setup() {
        mealsTableDelegate.tableView = mealsTableView
        exercisesTableDelegate.tableView = exerciseTableView
        mealsTableHeight.constant = 0
        exerciseTableHeight.constant = 0
        
        self.dateView.select(date: CalcaSettings.shared.selectedCalendarDate)
        self.loadActivities(date: CalcaSettings.shared.selectedCalendarDate)
    }
    
    var showMeals = false {
        didSet { updateMealsTableView() }
    }
    
    func updateMealsTableView() {
        let height: CGFloat = showMeals ? mealsTableDelegate.contentHeight : 0
        mealsTableHeight.setValueAnimated(height, scrollView)
        mealsTableView.show(animated: showMeals)
    }
    
    @IBAction func showMealsList(_ sender: Any) {
        if let count = userActivities?.meals?.count, count > 0 {
            NotificationCenter.post(SWIPE_CELL_EVENT)
            showMeals = !showMeals
        }
    }
    
    var showExercises = false {
        didSet { updateExercisesTableView() }
    }
    
    func updateExercisesTableView() {
        let height: CGFloat = self.showExercises ? exercisesTableDelegate.contentHeight : 0
        self.exerciseTableHeight.setValueAnimated(height, self.scrollView)
        self.exerciseTableView.show(animated: self.showExercises)
    }
    
    @IBAction func showExerciseList(_ sender: Any) {
        if let count = userActivities?.exercises?.count, count > 0 {
            NotificationCenter.post(SWIPE_CELL_EVENT)
            showExercises = !showExercises
        }
    }
    
    func updateButtons() {
        if let mealsCount = userActivities?.meals?.count {
            mealsButton.activate(mealsCount > 0)
            mealsButton.dropped = mealsCount > 0 && showMeals
        }
        
        if let exerciseCount = userActivities?.exercises?.count {
            exerciseButton.activate(exerciseCount > 0)
            exerciseButton.dropped = exerciseCount > 0 && showExercises
        }
    }
    
    func loadActivities(date: Date) {
        UserActivities.fetch(date: date) { (activities) -> (Void) in
            self.userActivities = activities
        }
    }
}

extension CalculatorController: DateViewDelegate {
    func dateSelected(_ date : Date) {
        CalcaSettings.shared.selectedCalendarDate = date
    }
}
