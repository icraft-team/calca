//
//  CalculatorControllerEvents.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 06/03/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

import UIKit

extension CalculatorController {
    func subscribeToEvents() {
        subscribe(DELETE_MEAL_FROM_CELL, #selector(deleteMealFromCell(_:)))
        subscribe(DELETE_EXERCISE_FROM_CELL, #selector(deleteExerciseFromCell(_:)))
        subscribe(ADD_MEAL_EVENT, #selector(loadActivitiesForSelectedDate))
        subscribe(ADD_EXERCISE_EVENT, #selector(loadActivitiesForSelectedDate))
        subscribe(SELECTED_DATE_CHANGED, #selector(selectedDateChanged))
    }
    
    @objc func deleteMealFromCell(_ notification : NSNotification) {
        guard let userInfo = notification.userInfo else { return }
        let meal : Meal = userInfo["object"] as! Meal

        mealsTableDelegate.delete(meal)
        userActivities?.meals?.remove(object: meal)
        meal.delete()

        metricsView.update()
        mealsTableHeight.setValueAnimated(mealsTableDelegate.contentHeight, self.scrollView)
    }
    
    @objc func deleteExerciseFromCell(_ notification : NSNotification) {
        guard let userInfo = notification.userInfo else { return }
        let exercise : PerformedExercise = userInfo["object"] as! PerformedExercise
        
        exercisesTableDelegate.delete(exercise: exercise)
        userActivities?.exercises?.remove(object: exercise)
        exercise.delete()
        
        metricsView.update()
        exerciseTableHeight.setValueAnimated(exercisesTableDelegate.contentHeight, self.scrollView)
    }
    
    @objc func selectedDateChanged() {
        dateView.select(date: CalcaSettings.shared.selectedCalendarDate)
        loadActivities(date: CalcaSettings.shared.selectedCalendarDate)
    }
    
    @objc func loadActivitiesForSelectedDate() {
        loadActivities(date: CalcaSettings.shared.selectedCalendarDate)
    }
}
