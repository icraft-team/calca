//
//  AchievementsController.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 23/03/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

import UIKit

class AchievementsController: NiceViewController {
    @IBOutlet weak var tableView: UITableView!
    
    let tableDelegate: AchievementTableDelegate = AchievementTableDelegate()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableDelegate.tableView = tableView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AchievementStore.fetch {
            guard let achievements = $0?.list else { return }
            self.tableDelegate.achievements = achievements
        }
    }
}
