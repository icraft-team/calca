//
//  StatsController.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 16/03/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

import UIKit
import Charts

class StatsController: NiceViewController {
    @IBOutlet weak var activitiesChart: ActivitiesBarChart!
    @IBOutlet weak var periodSegmentedControl: UISegmentedControl!
    @IBOutlet weak var datesFieldsY: NSLayoutConstraint!
    @IBOutlet weak var datesView: NiceView!
    @IBOutlet weak var datePickerView: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var fromDateField: SmartField!
    @IBOutlet weak var toDateField: SmartField!
    weak var selectedField: SmartField?
    
    enum Period: Int {
        case week = 0
        case month
        case year
        case custom
    }
//    
//    var customDaysCount: Int {
//        guard let fromDate = fromDateField.data as? Date else { return 0 }
//        guard let toDate = toDateField.data as? Date else { return 0 }
//        guard let toDateEnd = toDate.endOfDay else { return 0 }
//        let secondsBetween = toDateEnd.timeIntervalSince1970 - fromDate.startOfDay.timeIntervalSince1970
//        return Int(secondsBetween / 86400)
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup() {
        setupDatesView()
        
        periodSegmentedControl.removeDividers()
        periodSegmentedControl.setFont("Open Sans", state: UIControlState.normal)
        periodSegmentedControl.setFont("Open Sans", state: UIControlState.selected, color: .white)
        
        
        if let limit = AuthManager.get.profile?.user?.caloriesPerDay {
            activitiesChart.addLimit(limit)
        }
    }
    
    func setupDatesView() {
        datePicker.maximumDate = Date()
        datePickerView.hide()
        fromDateField.highlight(false)
        toDateField.highlight(false)
        datesFieldsY.constant = -40
        datesView.hide()
        
        let parseClosure: (Any) -> (String) = { (data) -> String in
            let date = data as! Date
            return Utils.getStringFrom(date, withFormat: "dd.MM.YYYY")
        }
        fromDateField.howToParseData = parseClosure
        toDateField.howToParseData = parseClosure
        fromDateField.data = Date().subtractDays(3)
        toDateField.data = Date()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadActivities()
    }

    @IBAction func periodControlChangedValue(_ sender: Any) {
        showDates = periodSegmentedControl.selectedSegmentIndex == 3 ? true : false
        loadActivities()
    }
    
    func fetchDates(for period: Period) -> (Date, Date) {
        if period == .custom {
            guard let start = fromDateField.data as? Date, let end = toDateField.data as? Date else {
                log("can't get dates from fields, check please")
                return (Date(), Date())
            }
            return (start, end)
        } else {
            let daysCountTable = [Period.year: 365,
                                  Period.month: 30,
                                  Period.week: 7]
            
            guard let daysCount = daysCountTable[period] else { return (Date(), Date()) }
            guard let startDate = Date().subtractDays(daysCount) else { return (Date(), Date()) }
            
            return (startDate, Date())
        }
    }
    
    func loadActivities() {
        let periodIndex = periodSegmentedControl.selectedSegmentIndex
        guard let period = Period(rawValue: periodIndex) else {
            log("cannot get value from period segmented control")
            return
        }
        let dates = fetchDates(for: period)
        
        UserActivities.fetchBetween(dates.0, and: dates.1) { (userActivities) -> (Void) in
            self.parseData(from: userActivities, for: dates)
        }
    }
    
    func parseData(from userActivities: UserActivities?, for dates: (Date, Date)) {
        guard let limit = AuthManager.get.profile?.user?.caloriesPerDay else { return }
        guard let activities = userActivities else { return }
        
        var iteration: Int = 0
        var yVals: [BarChartDataEntry] = [BarChartDataEntry]()
        while true {
            guard let date = dates.0.addDays(iteration), date.timeIntervalSince1970 <= dates.1.timeIntervalSince1970 else {
                break
            }
            var mealSum = Double(activities.mealSum(date.startOfDay, date.endOfDay))
            var exerciseSum = Double(activities.exerciseSum(date.startOfDay, date.endOfDay))
            let totalLimit = exerciseSum + Double(limit)
            
            var overLimit: Double = 0
            if mealSum > totalLimit {
                overLimit = mealSum - totalLimit
                mealSum -= overLimit
            }
            
            var exerciseMealInterception: Double = 0
            if mealSum > 0, exerciseSum > 0, mealSum - exerciseSum > 0 {
                exerciseMealInterception = exerciseSum
                mealSum -= exerciseSum
                exerciseSum = 0
            } else if mealSum - exerciseSum < 0 {
                exerciseMealInterception = mealSum
                exerciseSum = mealSum - exerciseSum
                mealSum = 0
            }
            
            let dataEntry = BarChartDataEntry(x: Double(iteration), yValues: [mealSum, exerciseMealInterception, exerciseSum, overLimit], icon: #imageLiteral(resourceName: "close1"))
            yVals.append(dataEntry)
            
            iteration += 1
        }
        
        activitiesChart.set(values: yVals)
    }
    
    var showDates: Bool = false {
        didSet {
            guard showDates != oldValue else { return }
            
            let y: CGFloat = showDates ? 8 : -40
            datesFieldsY.setValueAnimated(y)
            datesView.show(animated: showDates)
            if showDates { _ = fromDateField.becomeFirstResponder() }
        }
    }
    
    @IBAction func datePickerChangedValue(_ sender: Any) {
        let picker = sender as? UIDatePicker
        selectedField?.data = picker?.date
        loadActivities()
    }
    @IBAction func closeDatePickerView(_ sender: Any) {
        selectedField?.highlight(false)
        datePickerView.hideAnimated()
    }
}

extension StatsController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        fromDateField.highlight(false)
        toDateField.highlight(false)
        
        selectedField = textField as? SmartField
        selectedField?.highlight(true)
        datePickerView.showAnimated()
        return false
    }
}
