//
//  AddExerciseController.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 11/02/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

import UIKit

let ADD_EXERCISE_EVENT = "add exercise event"

class AddExerciseController: NiceViewController {
    @IBOutlet weak var formHeight: NSLayoutConstraint!
    @IBOutlet weak var form: UIView!
    @IBOutlet weak var exerciseField: UITextField!
    @IBOutlet weak var panelView: ModalView!
    @IBOutlet weak var ac: UIActivityIndicatorView!
    @IBOutlet weak var cancelButton: ShapedButton!
    @IBOutlet weak var calsBurnedLabel: UILabel!
    @IBOutlet weak var minutesOfExerciseField: UITextField!
    @IBOutlet weak var dateView: DateView!
    
    var autocompleteView : AutocompleteTableView?
    var exercises : [Exercise] = [Exercise]()
    var selectedExercise : Exercise? {
        didSet {
            if selectedExercise != nil {
                exerciseField.text = selectedExercise?.name
                expandForm()
            } else {
                shrinkForm()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        setup()
    }
    
    func setup() {
        shrinkForm()
        panelView.hide()
        panelView.unfade()
        dateView.select(date: CalcaSettings.shared.selectedCalendarDate)
        exerciseField.becomeFirstResponder()
    }
    
    @IBAction func exerciseFieldDidChange(_ sender: Any) {
        autocompleteExercise()
    }
    
    var timer: Timer?
    func autocompleteExercise() {
        selectedExercise = nil
        if timer != nil { timer?.invalidate()}
        timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false, block: { (timer) in
            self.getVariants()
        })
    }
    
    func getVariants() {
        guard let searchString = exerciseField.text else {
            log("exercise field returned nil value")
            return
        }
        
        if !exerciseField.isEmpty {
            ac.startAnimating()
            let url = REST.exerciseAutocomplete(searchString)
            get(url, { (response) in
                if let jsonArray = response?.jsonString?.toJSONArray() {
                    self.exercises = [Exercise].from(jsonArray: jsonArray)!
                    self.showAutoComplete()
                }
                self.ac.stopAnimating()
            })
        }
    }
    
    func showAutoComplete() {
        if (autocompleteView != nil) { autocompleteView?.removeFromSuperview() }
        autocompleteView = ExerciseAutocompleteTableView()
        autocompleteView?.delegate = self
        self.view.addSubview(autocompleteView!)
        autocompleteView?.unfade()
        _ = autocompleteView?.setVerticalMargin(4, to: self.panelView)
        _ = self.cancelButton?.setVerticalMargin(4, to: autocompleteView)
        _ = autocompleteView?.setLeading(-8, to: self.panelView)
        _ = autocompleteView?.setTrailing(8, to: self.panelView)
        autocompleteView?.variants = exercises
    }
    
    func expandForm() {
        if formHeight.constant == 96 { return }
        minutesOfExerciseField.becomeFirstResponder()
        self.formHeight.setValueAnimated(96, self.view)
        form.showAnimated()
        calculateBurnedCalories()
    }
    
    func shrinkForm() {
        if formHeight.constant == 0 { return }
        self.formHeight.setValueAnimated(0, self.view)
        form.hideAnimated()
    }
    
    func calculateBurnedCalories() {
        let weight = AuthManager.get.profile?.user?.weightInKilos
        guard let exercise = self.selectedExercise else { return }
        
        let minutes = minutesOfExerciseField.text?.toFloat
        if minutes != 0 {
            let hours = minutes! / 60
            let caloriesBurned = Int(weight! * exercise.MET! * hours)
            calsBurnedLabel.text = "minutes → \(caloriesBurned) kcals burnt"
        } else {
            calsBurnedLabel.text = "minutes → 0 kcals burnt"
        }
    }
    
    @IBAction func minutesFieldChanged(_ sender: Any) {
        calculateBurnedCalories()
    }
    @IBAction func minutesFieldBeginEditing(_ sender: Any) {
        selectedExercise = nil
        exerciseField.text = ""
    }
    
    override func close(_ sender: Any) {
        super.close()
        self.panelView.fade()
    }
    
    @IBAction func addExerciseButtonTapped(_ sender: Any) {
        guard let exercise = selectedExercise else { return }
        guard let minutes = minutesOfExerciseField.text?.toInt, minutes > 0 else { return }
        
        exercise.perform(minutes: minutes, date: CalcaSettings.shared.selectedCalendarDate) { (response) in
            self.log("successfully added exercise to selected day")
            NotificationCenter.post(ADD_EXERCISE_EVENT)
            self.dismiss(animated: true, completion: nil)
        }
    }
}

extension AddExerciseController : AutocompleteTableViewDelegate {
    func selectedRow(index: Int) {
        selectedExercise = exercises[index]
    }
    
    func autocompleteTableDidScroll() {
        exerciseField.resignFirstResponder()
    }
}

extension AddExerciseController: DateViewDelegate {
    func dateSelected(_ date : Date) {
        CalcaSettings.shared.selectedCalendarDate ≈ date
    }
}
