//
//  AuthController.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 30/04/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

import UIKit

class AuthController: NiceViewController {
    @IBOutlet weak var regLogin: UITextField!
    @IBOutlet weak var regPassword: UITextField!
    @IBOutlet weak var regRepeatPassword: UITextField!
    @IBOutlet weak var regEmail: UITextField!
    @IBOutlet weak var regPanel: UIView!
    
    @IBOutlet weak var loginPanel: UIView!
    @IBOutlet weak var authLogin: UITextField!
    @IBOutlet weak var authPassword: UITextField!

    @IBOutlet weak var yConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        regPanel.hide()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !AuthManager.get.authorized {
            LoadingBlocker.show(on: self.view, light: true, title: "Authorization")
            AuthManager.get.tryRestoreSession() { (restored) in
                if (!restored) {
                    LoadingBlocker.hide(from: self.view)
                    self.authLogin.becomeFirstResponder()
                } else {
                    self.segue("main")
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func showSignUp(_ sender: Any) {
        loginPanel.fade()
        regPanel.unfade()
        regLogin.becomeFirstResponder()
    }
    
    @IBAction func showSignIn(_ sender: Any) {
        loginPanel.unfade()
        regPanel.fade()
        authLogin.becomeFirstResponder()
    }
    
    @IBAction func login(_ sender: Any) {
        if (!super.checkFields()) { return }
        
        LoadingBlocker.show(on: self.regPanel, light: true)
        AuthManager.get.authorize(login: authLogin.text!, pass: authPassword.text!) { (response) in
            LoadingBlocker.hide(from: self.regPanel)
            if (response?.statusCode == 200) {
                self.segue("main")
            } else if (response?.statusCode == 401) {
                AlertManager.get().show(LOGIN_INVALID, in: self)
            }
        }
    }
    
    @IBAction func register() {
        if (!checkFields()) { return }
        
        LoadingBlocker.show(on: self.regPanel, light: true)
        unowned let selfWeak = self
        AuthManager.get.register(login: regLogin.text!, pass: regPassword.text!, email: regEmail.text!) { (response) in
            LoadingBlocker.hide(from: self.regPanel)
            if (response?.statusCode == 200) {
                selfWeak.showSignIn(selfWeak)
            } else if (response?.statusCode == 409) {
                AlertManager.get().show(LOGIN_TAKEN, in: self)
            } else if (response?.statusCode == 410) {
                AlertManager.get().show(EMAIL_TAKEN, in: self)
            }
        }
    }
    
    override func checkFields() -> Bool {
        if (regPassword.text != regRepeatPassword.text) { return false }
        return super.checkFields()
    }

}
