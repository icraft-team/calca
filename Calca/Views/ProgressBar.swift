//
//  ProgressBar.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 23/03/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

import UIKit

class ProgressBar: NiceView {

    @IBOutlet var bar: UIView!
    @IBOutlet var barWidth: NSLayoutConstraint!
    let progress: Float = 0
    
    func setProgress(_ progress: Float?, animated: Bool = false) {
        self.layoutIfNeeded()
        guard let progress = progress else { return }
        
        let width = self.width * CGFloat(progress)
        barWidth.constant = width
        if animated {
            UIView.animate(withDuration: 0.6) {
                self.layoutIfNeeded()
            }
        }
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
