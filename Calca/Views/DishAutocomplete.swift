//
//  MealAutocomplete.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 07/05/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

import UIKit

protocol DishAutocompleteDelegate {
    func didSelectVariant(_ index: Int)
}

class DishAutocomplete: UIScrollView {
    let rowHeight : CGFloat = 40
    let maximumHeight : CGFloat = 200
    var buttons : [UIButton] = [UIButton]()
    
    var acDelegate : DishAutocompleteDelegate?
    
    var nwidth = Utils.screenSize().width - 20 {
        didSet {
            self.frame.size.width = nwidth
            for button in buttons {
                button.frame.size.width = nwidth - 4
                let labels = button.subviews.filter{$0 is UILabel}
                labels[1].frame.origin.x = nwidth - 130
            }
        }
    }
    
    static func getWith(_ dishes: [Dish], _ delegate: DishAutocompleteDelegate) -> DishAutocomplete {
        let ac = DishAutocomplete()
        ac.clipsToBounds = false
        ac.layer.cornerRadius = 5
        ac.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        ac.acDelegate = delegate
        
        var i : CGFloat = 0
        for dish in dishes {
            let origin = CGPoint(x:2, y:i*ac.rowHeight + 2)
            let size = CGSize(width: ac.nwidth - 4, height:CGFloat(ac.rowHeight) - 4)
            let button = ShapedButton(frame: CGRect(origin:origin, size:size))
            button.color = UIColor.white
            button.tapColor = UIColor.lightGray
            button.backgroundColor = UIColor.white
            button.setTitleColor(UIColor.darkGray, for: UIControlState.normal)
            button.layer.cornerRadius = 3
            button.titleLabel?.font = UIFont(name: "Open Sans", size: 15)
            button.titleLabel?.numberOfLines = 0
            button.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
            button.contentHorizontalAlignment = .left
            button.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0)
            
            ac.buttons.append(button)
            button.setTitle(dish.name, for: UIControlState.normal)
            button.tag = Int(i)
            button.addTarget(ac, action: #selector(tapped(sender:)), for:.touchUpInside)
            
            if let calsPer = dish.calsPer {
                let calsLabel = UILabel.init(frame: CGRect(x: ac.nwidth - 130, y: size.height/2 - 15, width: 120, height: 30))
                calsLabel.font = UIFont(name: "Open Sans", size: 15)
                calsLabel.textColor = UIColor.darkGray
                calsLabel.textAlignment = NSTextAlignment.right
                if dish.singled! {
                    calsLabel.text = String(calsPer) + "kcal/serving"
                } else {
                    calsLabel.text = String(calsPer) + "kcal/100g"
                }
                button.addSubview(calsLabel)
            }
            
            ac.addSubview(button)
            i+=1
        }
        
        let height = i*ac.rowHeight > ac.maximumHeight ? ac.maximumHeight : i*ac.rowHeight
        ac.frame.size.height = height
        ac.contentSize = CGSize(width: ac.nwidth, height: CGFloat(i*ac.rowHeight))
        
        return ac
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder decoder: NSCoder) {
        super.init(coder: decoder)
    }
    
    @objc func tapped(sender : UIButton) {
        if let delegate = acDelegate {
            delegate.didSelectVariant(sender.tag)
        }
        close()
    }
    
    func addTo(view: UIView, anchor: UIView) {
        nwidth = anchor.frame.size.width
        if let point = anchor.superview?.convert(anchor.frame.origin, to: view) {
            self.frame.origin.x = point.x
            self.frame.origin.y = point.y + anchor.frame.size.height + 4
        }
        
        view.addSubview(self)
        
        var i : Double = 0
        for button in buttons {
            button.hide()
            let endOrigin = button.frame.origin
            button.frame.origin = CGPoint(x:2, y:0)
            
            UIView.animate(withDuration: 0.4, delay: 0.05*i, usingSpringWithDamping:0.8, initialSpringVelocity: 3, options: .curveEaseInOut, animations: {
                button.alpha = 1
                button.frame.origin = endOrigin
            }, completion: nil)
            i+=1
        }
    }
    
    func close() {
        self.hideAnimated { 
            self.removeFromSuperview()
        }
    }
}
