//
//  ExerciseAutocompleteCell.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 02/03/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

import UIKit

class ExerciseAutocompleteCell: AutocompleteCell {
    @IBOutlet weak var metImage: UIImageView!
    @IBOutlet weak var metLabel: UILabel!
    override func configure(with: Any) {
        let exercise = with as! Exercise
        self.titleLabel.text = exercise.name?.capitalizingFirstLetter()
        self.layoutIfNeeded()
        self.titleLabel.sizeToFit()
        
        guard let MET = exercise.MET else { return }
        self.metImage.image = imageFor(MET: MET)
        self.metLabel.text = "MET \(MET)"
    }
    
    func imageFor(MET: Float) -> UIImage? {
        if MET <= 3 {
            return UIImage(named: "walk")
        } else if MET > 3, MET <= 6 {
            return UIImage(named: "run")
        } else if MET > 6 {
            return UIImage(named: "fast_run")
        } else {
            return nil
        }
    }
}
