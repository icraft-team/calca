//
//  ExerciseCell.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 06/03/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

import UIKit

let DELETE_EXERCISE_FROM_CELL = "delete exercise from cell"
class PerformedExerciseCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var caloriesBurntLabel: UILabel!
    @IBOutlet weak var deleteButton: ShapedButton!
    @IBOutlet weak var niceView: NiceView!
    
    let swipedX : CGFloat = -48
    var swiping : Bool = false
    var startX : CGFloat = 0
    var exercise : PerformedExercise?
    
    func configure(with exercise: PerformedExercise) {
        self.exercise = exercise
        nameLabel.text = exercise.name
        nameLabel.sizeToFit()
        
        caloriesBurntLabel.text = String(exercise.caloriesBurnt)
    }
    
    @objc func handlePan(recognizer: UIPanGestureRecognizer) {
        let value = recognizer.translation(in: self.contentView).x
        
        if recognizer.state == UIGestureRecognizerState.began {
            swiping = true
            NotificationCenter.post(SWIPE_CELL_EVENT)
        }
        
        niceView.frame.origin.x = self.startX + value
        
        if recognizer.state == UIGestureRecognizerState.ended {
            swiping = false
            var origingEndedX : CGFloat = 0
            var deleteButtonAlpha : CGFloat = 0
            if niceView.frame.origin.x < -25 { origingEndedX = swipedX; deleteButtonAlpha = 1 }
            if niceView.frame.origin.x > 0 { origingEndedX = 0; deleteButtonAlpha = 0 }
            
            UIView.animate(withDuration: 0.3, animations: {
                self.niceView.frame.origin.x = origingEndedX
                self.deleteButton.alpha = deleteButtonAlpha
            }, completion: { (completed) in
                self.startX = self.niceView.frame.origin.x
            })
        }
        
        if niceView.frame.origin.x > swipedX, niceView.frame.origin.x < 0 {
            let deleteButtonAlpha =  niceView.frame.origin.x / swipedX
            deleteButton.alpha = deleteButtonAlpha
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan(recognizer:)))
        niceView.addGestureRecognizer(panGestureRecognizer)
        
        self.subscribe(SWIPE_CELL_EVENT, #selector(otherCellSwiped))
    }
    
    @objc func otherCellSwiped() {
        if !swiping {
            UIView.animate(withDuration: 0.3, animations: {
                self.niceView.frame.origin.x = 0
            })
        }
    }
    
    @IBAction func deleteExercise(_ sender: Any) {
        UIView.animate(withDuration: 0.3, animations: {
            self.niceView.frame.origin.x = 0
            self.deleteButton.alpha = 0
        }, completion: { (completed) in
            guard let exercise = self.exercise else { return }
            NotificationCenter.post(DELETE_EXERCISE_FROM_CELL, exercise)
        })
    }
    
    static func heightFor(_ text: String) -> CGFloat {
        let titleWidth = Utils.screenSize().width - 16*2 - 8*2 - 70 - 8*2
        guard let font = UIFont(name: "Open Sans", size: 15) else { return 0 }
        
        let attributes: [NSAttributedStringKey: UIFont] = [NSAttributedStringKey.font: font]
        let expectedLabelRect = text.boundingRect(with: CGSize(width: titleWidth, height: CGFloat.greatestFiniteMagnitude),
                                                     options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                                     attributes: attributes,
                                                     context: nil)
        
        return expectedLabelRect.size.height + 24
    }
}
