//
//  AchievementCell.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 27/03/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

import UIKit

class AchievementCell: UITableViewCell {
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var progressBar: ProgressBar!
    @IBOutlet weak var rewardLabel: UILabel!
    
    func configure(with achievement: Achievement) {
        descriptionLabel.text = achievement.description
        progressBar.setProgress(achievement.progress, animated: true)
        
        if let reward = achievement.goldReward {
            rewardLabel.text = "\(reward)"
        }
        
        titleLabel.text = achievement.name
    }
}
