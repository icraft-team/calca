//
//  MealCell.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 09/05/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

import UIKit

let SWIPE_CELL_EVENT = "swipe cell event"
let DELETE_MEAL_FROM_CELL = "delete meal from cell"

class MealCell: UITableViewCell {

    @IBOutlet weak var totalCals: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var portionDescription: UILabel!
    @IBOutlet weak var niceView: NiceView!
    @IBOutlet weak var deleteButton: ShapedButton!
    
    let swipedX : CGFloat = -48
    var swiping : Bool = false
    var startX : CGFloat = 0
    var meal : Meal?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan(recognizer:)))
        niceView.addGestureRecognizer(panGestureRecognizer)
        
        self.subscribe(SWIPE_CELL_EVENT, #selector(otherCellSwiped))
    }

    func configure(with: Meal) {
        meal = with
        totalCals.text = String(with.totalCals) + "kcal"
        
        guard let dishName = with.dishName else { return }
        guard let singled = with.singled else { return }
        guard let number = with.numberOfGramsOrServings else { return }
        let casedServing = String.caseFor(number, form1: "serving", form2: "servings", form5: "servings")
        let unitsString = singled ? "\(number) \(casedServing)" : "\(number)g"
        descriptionLabel.text = dishName
        portionDescription.text = unitsString
    }
    
    @objc func handlePan(recognizer: UIPanGestureRecognizer) {
        let value = recognizer.translation(in: self.contentView).x
        
//        if value < -50, startX == 0 { value = -50 }
//        if value > 0, startX == 0 { value = 0 }
//        if value > 50, startX == -50 { value = 50 }
        
        if recognizer.state == UIGestureRecognizerState.began {
            swiping = true
            NotificationCenter.post(SWIPE_CELL_EVENT)
        }
        
        niceView.frame.origin.x = self.startX + value
        
        if recognizer.state == UIGestureRecognizerState.ended {
            swiping = false
            var origingEndedX : CGFloat = 0
            var deleteButtonAlpha : CGFloat = 0
            if niceView.frame.origin.x < -25 { origingEndedX = swipedX; deleteButtonAlpha = 1 }
            if niceView.frame.origin.x > 0 { origingEndedX = 0; deleteButtonAlpha = 0 }
            
            UIView.animate(withDuration: 0.3, animations: {
                self.niceView.frame.origin.x = origingEndedX
                self.deleteButton.alpha = deleteButtonAlpha
            }, completion: { (completed) in
                self.startX = self.niceView.frame.origin.x
            })
        }
        
        if niceView.frame.origin.x > swipedX, niceView.frame.origin.x < 0 {
            let deleteButtonAlpha =  niceView.frame.origin.x / swipedX
            deleteButton.alpha = deleteButtonAlpha
        }
    }
    
    @IBAction func deleteMeal(_ sender: Any) {
        UIView.animate(withDuration: 0.3, animations: {
            self.niceView.frame.origin.x = 0
            self.deleteButton.alpha = 0
        }, completion: { (completed) in
            guard let meal = self.meal else { return }
            NotificationCenter.post(DELETE_MEAL_FROM_CELL, meal)
        })
    }
    
    @objc func otherCellSwiped() {
        if !swiping {
            UIView.animate(withDuration: 0.3, animations: {
                self.niceView.frame.origin.x = 0
            })
        }
    }
    
    static func heightFor(_ text: String) -> CGFloat {
        let titleWidth = Utils.screenSize().width - 16*2 - 8*2 - 70 - 8*2
        guard let font = UIFont(name: "Open Sans", size: 15) else { return 0 }
        
        let attributes: [NSAttributedStringKey: Any] = [NSAttributedStringKey.font: font]
        let expectedLabelRect = text.boundingRect(with: CGSize(width: titleWidth, height: CGFloat.greatestFiniteMagnitude),
                                                  options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                                  attributes: attributes,
                                                  context: nil)
        
        return expectedLabelRect.size.height + 40
    }
}
