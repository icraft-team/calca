//
//  CaloriesLimitCard.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 18/01/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

import UIKit

class CaloriesLimitCard: Card {
    @IBOutlet var fields : [UITextField] = []
    
    @IBOutlet weak var ageField: SmartField!
    @IBOutlet weak var activityField: SmartField!
    @IBOutlet weak var targetField: SmartField!
    
    @IBOutlet weak var weightPoundsField: SmartField!
    @IBOutlet weak var heightFeetField: SmartField!
    @IBOutlet weak var heightInchesField: SmartField!
    
    @IBOutlet weak var weightKgsField: SmartField!
    @IBOutlet weak var heightCmField: SmartField!
    
    @IBOutlet weak var metricUnitsView: UIView!
    @IBOutlet weak var imperialUnitsView: UIView!
    
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var caloriesIntakeLabel: UILabel!
    @IBOutlet weak var caloriesTitleLabel: UILabel!
    
    var calculatedIntake : Int = 0
    
    var activityMods : [Float] = [1.2, 1.4, 1.6]
    var targetMods : [Float] = [0.85, 1, 1.3]
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setup()
    }
    
    override func setup() {
        super.setup()
        ageField.addTarget(self, action: #selector(calculate), for: UIControlEvents.editingChanged)
        weightKgsField.addTarget(self, action: #selector(calculate), for: UIControlEvents.editingChanged)
        weightPoundsField.addTarget(self, action: #selector(calculate), for: UIControlEvents.editingChanged)
        heightCmField.addTarget(self, action: #selector(calculate), for: UIControlEvents.editingChanged)
        heightFeetField.addTarget(self, action: #selector(calculate), for: UIControlEvents.editingChanged)
        heightInchesField.addTarget(self, action: #selector(calculate), for: UIControlEvents.editingChanged)
        
        activityField.sfDelegate = self
        targetField.sfDelegate = self
        
        caloriesIntakeLabel.hide()
        caloriesTitleLabel.hide()
        okButton.hide()
        
        activityField.variants = ["Sedentary job", "Moderately active", "Vigorously active"]
        targetField.variants = ["Fat loss", "Maintenance", "Muscle gains"]
    }
    
    @IBOutlet weak var sexSwitch: UISegmentedControl!
    @IBAction func sexChanged(_ sender: Any) {
        calculate()
    }
    
    @IBOutlet weak var unitSystemSwitch: UISegmentedControl!
    @IBAction func unitsTypeChanged(_ sender: Any) {
        if unitSystemSwitch.selectedSegmentIndex == 0 {
            imperialUnitsView.show()
            metricUnitsView.hide()
        } else {
            imperialUnitsView.hide()
            metricUnitsView.show()
        }
        
        calculate()
    }
    
    func checkFields() -> Bool {
        var ok = true
        for field : UITextField in fields {
            if field.text?.count == 0, (field.superview?.layer.opacity)! > Float(0) {
//                field.bounce()
                ok = false
            }
        }
        return ok
    }
    
    @IBAction func okGo(_ sender: Any) {
        AuthManager.get.profile?.user?.caloriesPerDay = calculatedIntake
        AuthManager.get.profile?.user?.sync()
        NotificationCenter.post(PROFILE_UPDATE_EVENT)
        delegate?.finished(card: self)
    }
}

// formula
// men
// 10 x weight (kg) + 6.25 x height (cm) – 5 x age (y) + 5
// women
// 10 x weight (kg) + 6.25 x height (cm) – 5 x age (y) - 161
extension CaloriesLimitCard {
    @objc func calculate() {
        if checkFields() {
            let wh = getWeightAndheight()
            let weight = wh.0
            let height = wh.1
            let age = Float(ageField.text!)!
            let activity = activityField.selectedIndex()
            let target = targetField.selectedIndex()
            
            let sexAddition : Float = sexSwitch.selectedSegmentIndex == 0 ? 5 : -161
            let activityMod = activityMods[activity]
            let targetMod = targetMods[target]
            
            var intake = weight*10
            intake += height*6.25
            intake -= 5*age
            intake += sexAddition
            intake *= activityMod
            intake *= targetMod
            
            if intake > 0, intake < 25000 {
                calculatedIntake = Int(intake)
                okButton.show()
                caloriesIntakeLabel.show()
                caloriesTitleLabel.show()
                caloriesIntakeLabel.text = String(Int(intake))
            }
        } else {
            caloriesIntakeLabel.hide()
            caloriesTitleLabel.hide()
            okButton.hide()
        }
    }
    
    func getWeightAndheight() -> (Float, Float) {
        if unitSystemSwitch.selectedSegmentIndex == 0 {
            let weight = Float(weightPoundsField.text!)! * 2.2
            let height = Float(heightFeetField.text!)! * 30.48 + Float(heightInchesField.text!)! * 2.54
            return (weight, height)
        } else {
            let weight = Float(weightKgsField.text!)!
            let height = Float(heightCmField.text!)!
            return (weight, height)
        }
    }
}

extension CaloriesLimitCard : SmartFieldDelegate {
    func chosenVariant(variant: String) {
        calculate()
    }
}
