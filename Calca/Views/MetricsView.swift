
//  MetricsView.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 08/03/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

import UIKit

class MetricsView: NiceView {
    @IBOutlet weak var exerciseBarWidth: NSLayoutConstraint!
    @IBOutlet weak var overLimitBarWidth: NSLayoutConstraint!
    @IBOutlet weak var overLimitLabel: UILabel!
    @IBOutlet weak var mealBarWidth: NSLayoutConstraint!
    @IBOutlet weak var exercisesCaloriesLabel: UILabel!
    @IBOutlet weak var mealsCaloriesLabel: UILabel!
    @IBOutlet weak var remainingSlider: FTSlider!
    @IBOutlet weak var totalSlider: FTSlider!
    @IBOutlet weak var interceptionView: UIView!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var totalView: UIView!
    @IBOutlet weak var totalSliderHeight: NSLayoutConstraint!
    @IBOutlet weak var remainingSliderHeight: NSLayoutConstraint!
    
    var mealsCaloriesSum: CGFloat?
    var exercisesCaloriesSum: CGFloat?
    var caloriesLimit: CGFloat = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        totalSlider.hide()
        remainingSlider.hide()
        mealBarWidth.constant = 0
        exerciseBarWidth.constant = 0
        overLimitBarWidth.constant = 0
        totalSliderHeight.constant = 0
        remainingSliderHeight.constant = 0
        
        guard let patternImage = UIImage(named:"foodExerciseInterceptionBig") else { return }
        interceptionView.backgroundColor = UIColor(patternImage: patternImage)
    }
    
    func configure(userActivities: UserActivities, caloriesLimit: Int?) {
        guard let caloriesLimit = caloriesLimit else { log("calories limit is nil, nothing to draw"); return }
        self.caloriesLimit = CGFloat(caloriesLimit)
        self.userActivities = userActivities
        update()
    }
    
    var userActivities: UserActivities? {
        didSet {
            calculateSums()
        }
    }
    
    func calculateSums() {
        mealsCaloriesSum = userActivities?.meals?.reduce(0, { $0 + CGFloat($1.totalCals) })
        exercisesCaloriesSum = userActivities?.exercises?.reduce(0, { $0 + CGFloat($1.caloriesBurnt) })
    }
    
    func update() {
        calculateSums()
        
        guard let exercisesCaloriesSum = self.exercisesCaloriesSum else { return }
        guard let mealsCaloriesSum = self.mealsCaloriesSum else { return }
        let totalLimit = caloriesLimit + exercisesCaloriesSum
        let mealBarWidth = self.width * (mealsCaloriesSum/totalLimit)
        let exerciseBarWidth = self.width * (exercisesCaloriesSum/caloriesLimit)
        exercisesCaloriesLabel.show(animated: exercisesCaloriesSum > 0)
        if mealsCaloriesSum > totalLimit {
            let overLimitCalories = mealsCaloriesSum - totalLimit
            overLimitLabel.text = "+\(Int(overLimitCalories))"
            let overLimitBarWidth = self.width * (overLimitCalories / (totalLimit + overLimitCalories))
            self.overLimitBarWidth.constant = overLimitBarWidth
        } else {
            self.overLimitBarWidth.constant = 0
        }
        
        self.mealBarWidth.constant = mealBarWidth
        self.exerciseBarWidth.constant = exerciseBarWidth
        
        UIView.animate(withDuration: 0.4) {
            self.layoutIfNeeded()
        }
        
        totalSlider.titleLabel.text = "Total limit: \(Int(totalLimit))kcal"
        remainingSlider.titleLabel.text = "Remaining: \(Int(totalLimit - mealsCaloriesSum))"
        mealsCaloriesLabel.text = "\(Int(mealsCaloriesSum))"
        exercisesCaloriesLabel.text = "\(Int(exercisesCaloriesSum))"
        totalLabel.text = totalSlider.titleLabel.text
        
        updateTotalSlider()
        updateRemainingSlider()
    }
    
    func updateTotalSlider() {
        guard let exercisesCaloriesSum = self.exercisesCaloriesSum else { return }
        guard let mealsCaloriesSum = self.mealsCaloriesSum else { return }
        guard let viewToLayout = self.superview?.superview else { return }
        if mealsCaloriesSum > 0 || exercisesCaloriesSum > 0 {
            totalView.hideAnimated()
            totalSlider.showAnimated()
            totalSliderHeight.setValueAnimated(28, viewToLayout)
        } else {
            totalView.showAnimated()
            totalSlider.hideAnimated()
            totalSliderHeight.setValueAnimated(0, viewToLayout)
        }
    }
    
    func updateRemainingSlider() {
        guard let exercisesCaloriesSum = self.exercisesCaloriesSum else { return }
        guard let mealsCaloriesSum = self.mealsCaloriesSum else { return }
        guard let viewToLayout = self.superview?.superview else { return }
        
        let totalLimit = caloriesLimit + exercisesCaloriesSum
        
        if mealsCaloriesSum > 0, mealsCaloriesSum < totalLimit {
            remainingSlider.showAnimated()
            remainingSliderHeight.setValueAnimated(28, viewToLayout)
        } else {
            remainingSlider.hideAnimated()
            remainingSliderHeight.setValueAnimated(0, viewToLayout)
        }
    }
}
