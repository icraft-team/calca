//
//  ExerciseAutocompleteView.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 02/03/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

import UIKit

class ExerciseAutocompleteTableView: AutocompleteTableView {
    override func registerCell(_ tableView: UITableView) {
        tableView.register(UINib(nibName: "ExerciseAutocompleteCell", bundle: Bundle.main), forCellReuseIdentifier: "ExerciseAutocompleteCell")
    }
    
    override func getCellFor(_ indexPath: IndexPath) -> AutocompleteCell {
        var cell : ExerciseAutocompleteCell? = tableView.dequeueReusableCell(withIdentifier: "ExerciseAutocompleteCell") as! ExerciseAutocompleteCell?
        
        if (cell == nil) {
            cell = ExerciseAutocompleteCell(style: .subtitle, reuseIdentifier: "ExerciseAutocompleteCell")
        }
        
        if let variant = variants[indexPath.row], let cell = cell {
            cell.configure(with: variant)
        }
        
        return cell!
    }

}
