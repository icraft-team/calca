//
//  ModalView.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 01/05/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

import UIKit

class ModalView: NiceView {

    @IBInspectable var animated : Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if animated {
            
        }
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
