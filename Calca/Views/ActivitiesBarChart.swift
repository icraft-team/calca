//
//  ActivitiesBarChart.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 16/03/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

import UIKit
import Charts

class ActivitiesBarChart: NiceView {
    let allowMultipleLimitLines = false
    
    @IBOutlet weak var barChart: BarChartView!
    
    lazy var formatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.maximumFractionDigits = 1
        formatter.negativeSuffix = " kcal"
        formatter.positiveSuffix = " kcal"
        
        return formatter
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        barChart.chartDescription?.enabled = false
        barChart.dragEnabled = false
        barChart.setScaleEnabled(false)
        barChart.pinchZoomEnabled = false
        barChart.gridBackgroundColor = .clear

        barChart.xAxis.labelPosition = XAxis.LabelPosition.bottom
        barChart.leftAxis.axisLineColor = UIColor.lightGray
        barChart.leftAxis.gridColor = UIColor.lightGray.withAlphaComponent(0.5)
        barChart.xAxis.axisLineColor = UIColor.lightGray
        barChart.xAxis.gridColor = UIColor.lightGray.withAlphaComponent(0.5)
        barChart.rightAxis.axisLineColor = UIColor.lightGray.withAlphaComponent(0.5)
        barChart.rightAxis.gridColor = UIColor.lightGray.withAlphaComponent(0.5)
        barChart.rightAxis.enabled = false
    }

    func addLimit(_ limit: Int) {
        if !allowMultipleLimitLines { barChart.leftAxis.removeAllLimitLines() }
        
        let limitLine = ChartLimitLine(limit: Double(limit), label: "")
        limitLine.lineWidth = 1
        limitLine.lineDashLengths = [5, 5]
        limitLine.labelPosition = .rightTop
        if let font = UIFont(name: "Open Sans", size: 14) {
            limitLine.valueFont = font
        } 
        barChart.leftAxis.addLimitLine(limitLine)
    }
    
    func set(values: [BarChartDataEntry]) {
        let set = BarChartDataSet(values: values, label: nil)
        set.drawIconsEnabled = false
        set.drawValuesEnabled = false
        set.colors = [Utils.getColorFrom(MEALS_COLOR), incterceptionColor, Utils.getColorFrom(EXERCISE_COLOR), Utils.getColorFrom(OVER_LIMIT_COLOR)]
        set.stackLabels = ["Meals", "Burnt", "Exercises", "Over limit"]
        
        let data = BarChartData(dataSet: set)
        data.setValueFont(.systemFont(ofSize: 7, weight: .light))
        data.setValueFormatter(DefaultValueFormatter(formatter: formatter))
        data.setValueTextColor(.darkGray)
        
        barChart.fitBars = true
        
        barChart.data = data
        barChart.animate(yAxisDuration: 0.5)
    }
    
    var incterceptionColor: UIColor {
        guard let patternImage = UIImage(named:"foodExerciseInterceptionBig") else { return UIColor.black }
        return UIColor(patternImage: patternImage)
    }
}
