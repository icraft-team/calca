//
//  DateView.swift
//  Calca
//
//  Created by Valentin Cherepyanko on 25/11/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

import UIKit
import AKPickerView
import FSCalendar

@objc protocol DateViewDelegate {
    func dateSelected(_ date : Date)
}

class DateView: NiceView {
    @IBOutlet weak var headerButton: DropdownButton!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var selfHeight: NSLayoutConstraint!
    @IBOutlet weak var delegate: DateViewDelegate!
    
    var calendarExpanded: Bool = false {
        didSet {
            calendar.show(animated: calendarExpanded)
            let height: CGFloat = calendarExpanded ? 300 : 40
            selfHeight.constant = height
            
            UIView.animate(withDuration: 0.4, animations: {
                self.superview!.superview!.layoutIfNeeded()
            })
        }
    }
    
    override func setup() {
        super.setup()
        headerButton.activate(true)
        selfHeight.constant = 40
        setupCalendar()
    }
    
    func select(date: Date) {
        calendar.select(date, scrollToDate: true)
        if date.isToday { dateLabel.text = "Today" } else {
            dateLabel.text = date.stringWith(format: "d MMMM")
        }
    }
    
    @IBAction func headerTapped(_ sender: Any) {
        calendarExpanded = !calendarExpanded
    }
    
    @IBAction func leftTapped(_ sender: Any) {
        if let previousDay = calendar.selectedDate?.decrementedByDay {
            select(date: previousDay)
            if delegate != nil { delegate.dateSelected(previousDay) }
        }
    }
    
    @IBAction func rightTapped(_ sender: Any) {
        if let nextDay = calendar.selectedDate?.incrementedByDay {
            select(date: nextDay)
            if delegate != nil { delegate.dateSelected(nextDay) }
        }
    }
}

extension DateView: FSCalendarDelegate, FSCalendarDataSource, FSCalendarDelegateAppearance {
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillDefaultColorFor date: Date) -> UIColor? {
        return UIColor.clear
    }
    
    func setupCalendar() {
        calendar.locale = Locale(identifier: "en-US")
        calendar.scope = FSCalendarScope.month
        calendar.clipsToBounds = true
        calendar.hide()
        DispatchQueue.main.async {
            self.calendar.reloadData()
        }
        log("calendar setup")
        calendar.reloadData()
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        if date.isToday { dateLabel.text = "Today" } else {
            dateLabel.text = date.stringWith(format: "d MMMM")
        }
        
        if delegate != nil { delegate.dateSelected(date) }
    }
}
