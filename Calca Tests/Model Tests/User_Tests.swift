//
//  User_Tests.swift
//  Calca Tests
//
//  Created by Valentin Cherepyanko on 01/04/2018.
//  Copyright © 2018 Valentin Cherepyanko. All rights reserved.
//

import XCTest
@testable import Calca_Prod

class User_Tests: XCTestCase {
    
    var user: User!
    
    override func setUp() {
        super.setUp()
        
        do { try testJSONMapping() }
        catch { XCTFail("JSON mapping failed") }
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    func testJSONMapping() throws {
        guard let path = Bundle.main.path(forResource: "User", ofType: "json") else {
            XCTFail("Missing file: User.json")
            return
        }
        
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
            if let json = jsonResult as? JSON {
                user = User(json: json)
            }
        } catch {
            XCTFail("Can't read file with json")
        }
    }
    
    func testWeightCalculation() {
        XCTAssertEqual(user.weightInKilos, 75, "User has wrong weight calculation")
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        user = nil
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
