//
//  ShapedButton.h
//  DEM
//
//  Created by Valentin Cherepyanko on 19/12/14.
//
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@interface ShapedButton : UIButton

@property IBInspectable UIColor *color;
@property IBInspectable UIColor *tapColor;
@property IBInspectable CGFloat cornerRadius;

@end
