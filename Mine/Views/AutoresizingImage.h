//
//  AutoresizingImage.h
//  Conf
//
//  Created by Valentin Cherepyanko on 31/07/15.
//  Copyright (c) 2015 Valentin Cherepyanko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageProcessor.h"
#import <QuartzCore/QuartzCore.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface AutoresizingImage : UIImageView

- (void)changeImage:(UIImage *)image;
- (void)loadImage:(NSString *)urlString;

@property UIActivityIndicatorView *activityIndicator;

@end
