//
//  AutoComlpeteView.m
//  DEM
//
//  Created by Valentin Cherepyanko on 30/09/15.
//
//

#import "AutoComlpeteView.h"

@implementation AutoComlpeteView

- (void)awakeFromNib {
    [super awakeFromNib];
    [self hide];
    [self showAnimated];
}

+ (AutoComlpeteView *)initWith:(NSArray *)variants andCoords:(CGPoint)point andDelegate:(NSObject<AutoCompleteViewDelegate> *)delegate w:(CGFloat)width {
    NSUInteger count = [variants count];
    
    CGFloat height = count * 40 + 5;
    if (height > MAX_HEIGHT) height = MAX_HEIGHT;
    AutoComlpeteView *view = [[self alloc] initWithFrame:CGRectMake(point.x, point.y, width, height)];
    
    view.delegate = delegate;
    
    view.backgroundColor = [Utils getColorFromInt:MAIN_COLOR];
    view.clipsToBounds = YES;
    view.layer.cornerRadius = 3.0f;
    
    view.scrollView = [[UIScrollView alloc] initWithFrame:view.bounds];
//    view.scrollView.backgroundColor = [UIColor greenColor];
    [view addSubview:view.scrollView];
    
    int i = 0;
    for (NSString *variant in variants) {
//        if (i < 5) {
            ShapedButton *button = [[ShapedButton alloc] initWithFrame:CGRectMake(5, 40 * i + 5, width - 10, 35)];
            button.cornerRadius = 3;
            button.tag = i;
            button.backgroundColor = [UIColor whiteColor];
            button.tintColor = [Utils getColorFromInt:SECOND_COLOR];
            [button setTitleColor:[Utils getColorFromInt:SECOND_COLOR] forState:UIControlStateHighlighted];
            button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            button.contentEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
            
            button.titleLabel.font = [UIFont fontWithName:@"Open Sans" size:14];
            button.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
            
            [button setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            [button setTitle:variant forState:UIControlStateNormal];
            [button addTarget:view action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
            
            [view.scrollView addSubview:button];
            i++;
//        } else { break; }
    }
    [view.scrollView setContentSize:CGSizeMake(width, count * 40 + 5)];
    
    return view;
}

+ (AutoComlpeteView *)initWith:(NSArray *)variants andTextField:(UITextField *)textfield andDelegate:(NSObject<AutoCompleteViewDelegate> *)delegate {
    CGFloat x = textfield.frame.origin.x;
    CGFloat y = textfield.frame.origin.y + textfield.frame.size.height + 5;
    return [AutoComlpeteView initWith:variants andCoords:CGPointMake(x, y) andDelegate:delegate w:textfield.frame.size.width];
}

- (void)buttonTapped:(UIButton*)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didSelectVariant:)]) {
        [self.delegate didSelectVariant:sender.tag];
    }
    
    [self hideAnimatedWithCompletion:^{
        [self removeFromSuperview];
    }];
}

- (void)didSelectVariant:(NSUInteger)index {
    
}

@end
