//
//  CoolViewController.swift
//  KingMFC
//
//  Created by Valentin Cherepyanko on 19/04/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

import UIKit
import SideMenu

class NiceViewController: UIViewController, DropdownMenuDelegate {
    @IBInspectable var hideNavigationBar: Bool = false
    var menu : DropdownMenu? = nil
    
    var segueObjects : NSMutableDictionary! = NSMutableDictionary()
    
    override func viewDidLoad() {
//        createMenu()
    }
    
    func createMenu() {
        if (SideMenuManager.menuLeftNavigationController == nil) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "menuController")
            let menuLeftNavigationController = UISideMenuNavigationController(rootViewController:controller)
            menuLeftNavigationController.leftSide = true
            SideMenuManager.menuLeftNavigationController = menuLeftNavigationController
            
            SideMenuManager.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
            SideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
            
            SideMenuManager.menuPresentMode = .menuSlideIn
            SideMenuManager.menuAllowPushOfSameClassTwice = false
            SideMenuManager.menuAnimationUsingSpringWithDamping = 1
            SideMenuManager.menuAnimationOptions = .curveEaseInOut
            SideMenuManager.menuFadeStatusBar = false
            SideMenuManager.menuPushStyle = .defaultBehavior
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if (hideNavigationBar) {
            navigationController?.setNavigationBarHidden(hideNavigationBar, animated: true)
        }
    }
    
    func passData(_ object: Any?) {
        
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }

    // segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let toViewController = segue.destination as? NiceViewController {
            toViewController.passData(segueObjects.object(forKey: segue))
        }
    }
    
    func segue(_ segue: String) {
        self.segue(segue, object: nil)
    }
    
    func segue(_ segue: String, object: Any?) {
        if (object != nil) {
            segueObjects.setObject(object!, forKey: segue as NSCopying)
        }
        
        self.performSegue(withIdentifier: segue, sender: self)
    }
    
    @IBAction func showMenu(_ sender: Any) {
//        if (menu != nil) {
//            if (menu!.shown){
//                menu?.shrink()
//            } else {
//                menu!.expand()
//            }
//        } else {
//            menu = DropdownMenu.initWith(["Профиль", "Мои награды", "Коллектив"], sender as! UIButton, self, self.view)
//            navigationController?.view.addSubview(menu!)
//            //        self.view.addSubview(menu)
//            menu!.expand()
//        }
        
//        segue("menu")
        present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    func selected(index : Int) {
        
    }
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
//        self.navigationController?.popViewController(animated: true)
    }
}
