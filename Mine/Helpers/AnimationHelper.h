//
//  AnimationHelper.h
//  RutokenTest
//
//  Created by Valentin Cherepyanko on 12/03/15.
//  Copyright (c) 2015 Aktiv Co. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>
#import <math.h>
#import <POP/POP.h>
#import "Utils.h"

@interface AnimationHelper : NSObject

+ (void)rotateThis:(UIView *)view durationTime:(float)duration;
+ (void)stopAnimatingThis:(UIView *)view;
+ (void)hideThis:(UIView *)view delay:(float)delay durationTime:(float)duration callback:(void (^)(void))callback;
+ (void)showThis:(UIView *)view delay:(float)delay durationTime:(float)duration callback:(void (^)(void))callback;

+ (POPSpringAnimation *)move:(UIView *)view on:(CGPoint)point;
+ (POPSpringAnimation *)popupAnimation;
+ (POPSpringAnimation *)popdownAnimation;
+ (POPSpringAnimation *)bounceAnimation;
+ (void)xBounce:(UIView *)view;

+ (void)rotateThis:(UIView *)view durationTime:(float)duration byAngle:(id)angle;
+ (void)rotateUP:(UIView *)view duration:(float)duration;
+ (void)rotateDown:(UIView *)view duration:(float)duration;

@end
