//
//  ServerSettings.h
//  DEM
//
//  Created by Valentin Cherepyanko on 04/12/14.
//
//


//#define TimesheetID @"317d30ea-8fcd-7f78-82ee-208cc219f16d"
//#define ProductID @"c01f336a-100d-8ad7-480f-1564173765bb"
//#define SubjectID @"9cd7150a-0f30-2a00-3051-07d2308b8ce5"
//#define ProjectID @"3072c293-dc52-79e4-ca9c-4f1b37a2d880"

#define USER_ENTITY_ID @"14c3f639-676f-fb99-5dde-67c0e59b62c2"

//#define SERVER_ADDRESS @"gk-strizhi.ru"
#define SERVER_ADDRESS @"mfcgame.point2.at-sibir.ru/rest"
#define HTTP_PORT 0

#define CHECK_LOGIN_URL @"data/entity?entityId=14c3f639-676f-fb99-5dde-67c0e59b62c2&attributes=filial.organization.name,filial.name,lastName,firstName,secondName,email,phone&search="
#define LOGIN_URL @"login"
//#define PING_URL @"ping"
//#define CHECK_LOGIN_URL @"data/entity?entityId=0d4363dd-41fe-1248-b76d-e9b3bafadddf&attributes=login,fio,jobTitle.name&search="
//#define PROJECT_SEARCH_URL @"data/entity?entityId=3072c293-dc52-79e4-ca9c-4f1b37a2d880&attributes=code&search="
#define ENTITY_URL @"data/entity"
//#define ADD_WORKLOG_URL @"data/entity/30cf2ce4-4d4c-50f7-6f8c-04b308ea95d3"
//#define DELETE_ENTITY_URL @"data/deleteentity"


#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface ServerSettings : NSObject

+ (NSURL *)loginURL;
+ (NSURL *)checkLoginUrl:(NSString *)login;
+ (NSURL *)entityURL;

//+ (NSURL *)pingURL;
//+ (NSURL *)checkLoginUrl:(NSString *)login;
//+ (NSURL *)projectSearchURL:(NSString *)search;
//+ (NSURL *)addWorklogURL;
//+ (NSURL *)deleteEntityURL;
//+ (NSURL *)editEntityURL:(NSString *)objectID;
//+ (NSURL *)entitySearchURL:(NSString *)entityID search:(NSString *)search;
//+ (NSURL *)addObjectURL:(NSString *)objectID;

@end
