//
//  ServerSettings.m
//  DEM
//
//  Created by Valentin Cherepyanko on 04/12/14.
//
//

#import "ServerSettings.h"

@implementation ServerSettings

+ (NSString *)getServerAddressString {
    if (HTTP_PORT > 0 && HTTP_PORT < 65535) {
        return [NSString stringWithFormat:@"http://%@:%d", SERVER_ADDRESS, HTTP_PORT];
    } else {
        return [NSString stringWithFormat:@"http://%@", SERVER_ADDRESS];
    }
}

+ (NSURL *)loginURL {
    NSString *urlString = [NSString stringWithFormat:@"%@/%@", [self getServerAddressString], LOGIN_URL];
    return [NSURL URLWithString:urlString];
}

+ (NSURL *)checkLoginUrl:(NSString *)login {
    NSString *urlString = [NSString stringWithFormat:@"%@/%@%@", [self getServerAddressString], CHECK_LOGIN_URL, login];
    return [NSURL URLWithString:urlString];
}

+ (NSURL *)entityURL {
    NSString *urlString = [NSString stringWithFormat:@"%@/%@", [self getServerAddressString], ENTITY_URL];
    return [NSURL URLWithString:urlString];
}

//+ (NSURL *)pingURL {
//    NSString *urlString = [NSString stringWithFormat:@"%@/%@", [self getServerAddressString], PING_URL];
//    return [NSURL URLWithString:urlString];
//}
//
//+ (NSURL *)authURL {
//    NSString *urlString = [NSString stringWithFormat:@"%@/%@", [self getServerAddressString], AUTH_URL];
//    return [NSURL URLWithString:urlString];
//}
//
//+ (NSURL *)checkLoginUrl:(NSString *)login {
//    NSString *urlString = [NSString stringWithFormat:@"%@/%@%@", [self getServerAddressString], CHECK_LOGIN_URL, login];
//    return [NSURL URLWithString:urlString];
//}
//
//
//+ (NSURL *)addWorklogURL {
//    NSString *urlString = [NSString stringWithFormat:@"%@/%@", [self getServerAddressString], ADD_WORKLOG_URL];
//    return [NSURL URLWithString:urlString];
//}
//
//+ (NSURL *)projectSearchURL:(NSString *)search {
//    NSString *urlString = [NSString stringWithFormat:@"%@/%@%@", [self getServerAddressString], PROJECT_SEARCH_URL, search];
//    urlString = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
//    return [NSURL URLWithString:urlString];
//}
//
//+ (NSURL *)deleteEntityURL {
//    NSString *urlString = [NSString stringWithFormat:@"%@/%@", [self getServerAddressString], DELETE_ENTITY_URL];
//    urlString = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
//    return [NSURL URLWithString:urlString];
//}
//
//+ (NSURL *)editEntityURL:(NSString *)objectID {
//    NSString *urlString = [NSString stringWithFormat:@"%@/%@/%@", [self getServerAddressString], ENTITY_URL, objectID];
//    urlString = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
//    return [NSURL URLWithString:urlString];
//}
//
//+ (NSURL *)entitySearchURL:(NSString *)entityID search:(NSString *)search {
//    NSString *urlString = [NSString stringWithFormat:@"%@/%@?entityId=%@&search=%@", [self getServerAddressString], ENTITY_URL, entityID, search];
//    urlString = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
//    return [NSURL URLWithString:urlString];
//}
//
//+ (NSURL *)addObjectURL:(NSString *)objectID {
//    NSString *urlString = [NSString stringWithFormat:@"%@/%@/%@", [self getServerAddressString], ENTITY_URL, objectID];
//    urlString = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
//    return [NSURL URLWithString:urlString];
//}

@end
