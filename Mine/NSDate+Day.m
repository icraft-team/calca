//
//  NSDate+Day.m
//  Calca
//
//  Created by Valentin Cherepyanko on 10/02/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "NSDate+Day.h"

@implementation NSDate(Day)

- (NSDate*)day {
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
    NSDateComponents *comps = [[NSCalendar currentCalendar] components:unitFlags fromDate:self];
    comps.hour = 12;
    return [[NSCalendar currentCalendar] dateFromComponents:comps];
}

@end
